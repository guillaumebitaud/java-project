﻿Lancer le programme en exécutant la class BL2P

Afin de tester rapidement le fonctionnement de l'application, vous pouvez vous identifier en utilisant l'identifiant suivant :
	- identifiant : 	guillaume
	- mot de passe:		test

Ce programme utilise librement les bibliothèques JFreeChart pour la génération des graphiques
ainsi que iText pour la création des rapports au format PDF. 

Afin de respecter la licence AGPL imposée par l'utilisation de iText, le code source de l'application est disponible gratuitement. 