import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.RowSpec;
import com.jgoodies.forms.factories.FormFactory;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import java.awt.Font;
import javax.swing.JButton;
import java.util.Date;
import com.toedter.calendar.JDateChooser;
import java.awt.SystemColor;
import java.awt.Component;
import javax.swing.Box;


/**
 * Class affichant la fen�tre d'ajout d'un compte
 * @author Pierre LE POTTIER
 *
 */
public class AjouterCompteUI extends JFrame {

	// A utiliser dans le cas d'une s�rialisation de cette class
	private static final long serialVersionUID = -397637895343497482L;

	private JPanel contentPane;
	
	// TextField � remplir par l'utilisateur
	private JTextField tf_nom_compte;
	private JTextField tf_interet;
	private JTextField tf_solde_init;
	private JTextField tf_solde_minimal;
	
	// Date d'ouverture
	private JDateChooser dateChooser;
	
	// Bouton Cr�er un compte
	private JButton btnOk;
	
	// Attributs � transmettre
	private String nom_du_compte;
	private Date date_ouverture_compte;
	private double taux_dinteret;
	private double solde_initiale;
	private double solde_mini;
	private double prev;
	private JLabel lblDescription;
	private JTextField tf_description;
	private Component horizontalGlue;

	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AjouterCompteUI frame = new AjouterCompteUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public AjouterCompteUI() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setTitle("Ajouter un compte");
		setBounds(100, 100, 520, 314);
		contentPane = new JPanel();
		contentPane.setBackground(SystemColor.controlHighlight);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new FormLayout(new ColumnSpec[] {
				FormFactory.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("max(8dlu;default)"),
				FormFactory.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("max(7dlu;default)"),
				FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("max(37dlu;default):grow"),
				ColumnSpec.decode("max(11dlu;default)"),
				ColumnSpec.decode("max(37dlu;default):grow"),
				FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC,},
			new RowSpec[] {
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				RowSpec.decode("max(33dlu;default)"),
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				RowSpec.decode("max(29dlu;default):grow"),
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				RowSpec.decode("max(5dlu;default)"),
				FormFactory.DEFAULT_ROWSPEC,}));
		
	
		
		JLabel lbl_nom_compte = new JLabel("Nom du compte :");
		lbl_nom_compte.setHorizontalAlignment(SwingConstants.CENTER);
		lbl_nom_compte.setFont(new Font("Tahoma", Font.PLAIN, 12));
		contentPane.add(lbl_nom_compte, "5, 4, right, default");
		
		tf_nom_compte = new JTextField();
		tf_nom_compte.setColumns(10);
		contentPane.add(tf_nom_compte, "7, 4, 2, 1, fill, default");
		
		lblDescription = new JLabel("Description :");
		lblDescription.setFont(new Font("Tahoma", Font.PLAIN, 12));
		contentPane.add(lblDescription, "5, 6, right, default");
		
		tf_description = new JTextField();
		contentPane.add(tf_description, "7, 6, 3, 1");
		tf_description.setColumns(10);
		
		JLabel lbl_date_ouverture = new JLabel("Date de l'ouverture du compte :");
		lbl_date_ouverture.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lbl_date_ouverture.setHorizontalAlignment(SwingConstants.CENTER);
		contentPane.add(lbl_date_ouverture, "5, 7, right, default");
		
		dateChooser = new JDateChooser();
		contentPane.add(dateChooser, "7, 7, 2, 1, fill, center");
		
		JLabel lbl_taux_interet = new JLabel("Taux d'int\u00E9r\u00EAt :");
		lbl_taux_interet.setFont(new Font("Tahoma", Font.PLAIN, 12));
		contentPane.add(lbl_taux_interet, "5, 8, right, default");
		
		tf_interet = new JTextField();
		tf_interet.setColumns(10);
		
		contentPane.add(tf_interet, "7, 8, fill, top");		
		
		JLabel lblSoldeInitial = new JLabel("Solde initial :");
		lblSoldeInitial.setFont(new Font("Tahoma", Font.PLAIN, 12));
		contentPane.add(lblSoldeInitial, "5, 10, right, default");
		tf_solde_init = new JTextField();
		tf_solde_init.setColumns(10);
		contentPane.add(tf_solde_init, "7, 10, left, top");
		
		JLabel lbl_euro_2 = new JLabel("\u20AC");
		lbl_euro_2.setFont(new Font("Tahoma", Font.PLAIN, 12));
		contentPane.add(lbl_euro_2, "8, 10");
		
		horizontalGlue = Box.createHorizontalGlue();
		contentPane.add(horizontalGlue, "11, 10");
		
		JLabel lblDecouvertAutorise = new JLabel("Decouvert autorise");
		lblDecouvertAutorise.setFont(new Font("Tahoma", Font.PLAIN, 12));
		contentPane.add(lblDecouvertAutorise, "5, 12, right, default");
		
		tf_solde_minimal = new JTextField();
		tf_solde_minimal.setText("");
		tf_solde_minimal.setColumns(10);
		contentPane.add(tf_solde_minimal, "7, 12, left, default");
		
		JLabel lbl_euro = new JLabel("\u20AC");
		lbl_euro.setFont(new Font("Tahoma", Font.PLAIN, 12));
		contentPane.add(lbl_euro, "8, 12");
		
		btnOk = new JButton("Ajouter un compte");
		btnOk.setFont(new Font("Tahoma", Font.PLAIN, 13));
		
		contentPane.add(btnOk, "9, 15");
		
	}
	
	public JButton getBtnOk()
	{
		return btnOk;
	}
	
	/**
	 * M�thode retournant un objet de type Compte cr�� � partir des donn�es fournies par l'utilisateur
	 * @return Compte compte cr�er � l'aide de la fen�tre. 
	 */
	public Compte getCompte() {
		
		nom_du_compte = tf_nom_compte.getText();
		date_ouverture_compte = dateChooser.getDate();
		prev = 0;
		
		// V�rification des donn�es entr�es par l'utilisateur
		if(!tf_interet.getText().isEmpty())
			taux_dinteret = Double.parseDouble(tf_interet.getText());
		else
			taux_dinteret = 0;
		
		if(!tf_solde_init.getText().isEmpty())
			solde_initiale = Double.parseDouble(tf_solde_init.getText());
		else
			solde_initiale = 0;
		
		if(!tf_solde_minimal.getText().isEmpty())
			solde_mini = Double.parseDouble(tf_solde_minimal.getText());
		else
			solde_mini = 0;
		
		if ( (taux_dinteret <= 0))
		{
			// Instancier un nouveau compte courant
			Compte_Courant CCP = new Compte_Courant(nom_du_compte, tf_description.getText() , solde_initiale, date_ouverture_compte, solde_mini);
			return CCP;
		}
		else 
			{
			// Instancier un nouveau compte �pargne
			Compte_Epargne Livre_epargne = new Compte_Epargne(nom_du_compte,tf_description.getText(), solde_initiale,taux_dinteret,prev, date_ouverture_compte,solde_mini);
			return Livre_epargne;
			}
		
	}
	
	/**
	 * M�thode retournant le nom du compte cr��
	 * @return String le nom du compte
	 */
	public String getNomCompte(){
		return tf_nom_compte.getText();
	}

}
