import java.awt.Component;

import javax.swing.DefaultListCellRenderer;
import javax.swing.JList;

/**
 * Class permettant de redéfinir le rendu des Comptes dans les JComboBox
 * @author Guillaume BITAUD
 *
 */
public class ComboBoxRender extends DefaultListCellRenderer {
	
	// A utiliser dans le cas d'une sérialisation de cette class
	private static final long serialVersionUID = -3230727079280987577L;

	public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus){
	        if (value instanceof Compte) {
	            value = ((Compte)value).getName();
	        }
	        
	        super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
	        return this;
	    }
	}

