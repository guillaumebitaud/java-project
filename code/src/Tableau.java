import java.util.ArrayList;
import javax.swing.JScrollPane;
import javax.swing.JTable;

/**
 * 
 * @author Guillaume BITAUD
 * Class contenant tous les objets n�cessaires � l'affichage du tableau central de l'application
 */
public class Tableau {

	// Liste de compte
	private ArrayList<Compte> liste_comptes;
		
	// Liste permettant de contenir les ScrollPanel contenant les listes d'op�rations.
	private ArrayList<JScrollPane> liste_scroll;
	
	// Liste contenant une liste d'op�rations pour chaque Tableau 
	private ArrayList<JTable> tableau_operation;
	
	// Modele d'un tableau
	private ArrayList<ModeleData> donnees_tableau;	
	
	/**
	 * Constructeur par d�faut
	 */
	public Tableau(){
		// Instanciation des listes
		liste_scroll = new ArrayList<JScrollPane>();
		tableau_operation = new ArrayList<JTable>();
		liste_comptes = new ArrayList<Compte>();
		donnees_tableau = new ArrayList<ModeleData>();
		
	}
	
	
	public ArrayList<JScrollPane> getListe_scroll() {
		return liste_scroll;
	}
	public void setListe_scroll(ArrayList<JScrollPane> liste_scroll) {
		this.liste_scroll = liste_scroll;
	}
	public ArrayList<JTable> getTableau_operation() {
		return tableau_operation;
	}
	public void setTableau_operation(ArrayList<JTable> tableau_operation) {
		this.tableau_operation = tableau_operation;
	}
	public ArrayList<Compte> getListe_comptes() {
		return liste_comptes;
	}
	public void setListe_comptes(ArrayList<Compte> liste_comptes) {
		this.liste_comptes = liste_comptes;
	}
	public ArrayList<ModeleData> getDonnees_tableau() {
		return donnees_tableau;
	}
	public void setDonnees_tableau(ArrayList<ModeleData> donnees_tableau) {
		this.donnees_tableau = donnees_tableau;
	}
	
	
	
	
}
