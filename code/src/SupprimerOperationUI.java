import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.GridLayout;
import javax.swing.SwingConstants;

/**
 * Class permettant d'afficher la fen�tre de confirmation de suppression d'op�rations
 * @author Guillaume BITAUD
 *
 */
public class SupprimerOperationUI extends JDialog {

	// A utiliser dans le cas d'une s�rialisation de cette class
	private static final long serialVersionUID = 5264338429193116635L;
	
	private final JPanel contentPanel = new JPanel();
	
	private JButton okButton;
	private JButton cancelButton;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			SupprimerOperationUI dialog = new SupprimerOperationUI();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public SupprimerOperationUI() {
		setBounds(100, 100, 447, 210);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(new GridLayout(0, 1, 0, 0));
		{
			JLabel lbl_supprimer_operations = new JLabel("Voulez-vous vraiment supprimer \r\nles op\u00E9rations s\u00E9lectionn\u00E9es ?");
			lbl_supprimer_operations.setHorizontalAlignment(SwingConstants.CENTER);
			lbl_supprimer_operations.setFont(new Font("Tahoma", Font.PLAIN, 14));
			contentPanel.add(lbl_supprimer_operations);
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				okButton = new JButton("OK");
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				cancelButton = new JButton("Cancel");
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
	}

	public JButton getOkButton() {
		return okButton;
	}

	public JButton getCancelButton() {
		return cancelButton;
	}
	
	

}
