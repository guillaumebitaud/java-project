import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import com.toedter.calendar.JDateChooser;
import java.awt.Font;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.JTextField;
import javax.swing.JButton;
import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.RowSpec;
import java.util.Date;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import java.awt.SystemColor;

/**
 * Class permettant d'afficher la fen�tre d'ajout d'une operation
 * @author Pierre LE POTTIER
 *
 */
public class AjouterOperationUI extends JFrame {

	// A utiliser dans le cas d'une s�rialisation de cette class
	private static final long serialVersionUID = -1782406461952930945L;
	

	private JPanel contentPane;
	private JTextField tf_libelle;
	private boolean debit_credit;
	private JTextField tf_montant;
	private double montant;
	private String description;
	private Date date_operation;
	private String etiquette;
	private JButton btn_ok;
	private JDateChooser dateChooser;
	private JComboBox cb_etiquette;
	private JLabel lbl_euro;
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AjouterOperationUI frame = new AjouterOperationUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public AjouterOperationUI() {
		setTitle("Ajouter une op\u00E9ration");
		setBounds(100, 100, 500, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new FormLayout(new ColumnSpec[] {
				ColumnSpec.decode("121px"),
				ColumnSpec.decode("86px"),
				ColumnSpec.decode("1px"),
				FormFactory.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("78px:grow"),
				ColumnSpec.decode("18px"),
				FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC,
				FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
				ColumnSpec.decode("1px"),
				ColumnSpec.decode("6px"),
				FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
				ColumnSpec.decode("1px"),
				FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
				ColumnSpec.decode("59px"),
				FormFactory.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("47px"),
				FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
				ColumnSpec.decode("1px"),},
			new RowSpec[] {
				FormFactory.UNRELATED_GAP_ROWSPEC,
				RowSpec.decode("44px"),
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.PARAGRAPH_GAP_ROWSPEC,
				RowSpec.decode("1px"),
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				RowSpec.decode("default:grow"),
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.PARAGRAPH_GAP_ROWSPEC,
				RowSpec.decode("20px"),
				FormFactory.RELATED_GAP_ROWSPEC,
				RowSpec.decode("23px"),
				FormFactory.RELATED_GAP_ROWSPEC,
				RowSpec.decode("20px:grow"),
				FormFactory.DEFAULT_ROWSPEC,}));
		
		date_operation = new Date();
		
		JLabel lbl_ajouter_une_operation = new JLabel("Ajouter une op\u00E9ration :");
		lbl_ajouter_une_operation.setBackground(SystemColor.inactiveCaptionText);
		lbl_ajouter_une_operation.setForeground(SystemColor.desktop);
		lbl_ajouter_une_operation.setFont(new Font("Tahoma", Font.PLAIN, 20));
		contentPane.add(lbl_ajouter_une_operation, "1, 1, 4, 2");
		
		JLabel lbl_libelle = new JLabel("Libell\u00E9 :");
		lbl_libelle.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lbl_libelle.setHorizontalAlignment(SwingConstants.RIGHT);
		contentPane.add(lbl_libelle, "2, 4");
		
		tf_libelle = new JTextField();
		tf_libelle.setColumns(10);
		contentPane.add(tf_libelle, "5, 4, fill, default");

		
		JLabel lblEtiquette = new JLabel("Etiquette :");
		lblEtiquette.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblEtiquette.setHorizontalAlignment(SwingConstants.RIGHT);
		contentPane.add(lblEtiquette, "2, 8");
		

		cb_etiquette = new JComboBox();
		cb_etiquette.setModel(new DefaultComboBoxModel(new String[] {"SALAIRE", "PRIME", "TRANSPORT", "NOURRITURE", "LOGEMENT", "EPARGNE", "AUTRES"}));
		cb_etiquette.setToolTipText("");
		contentPane.add(cb_etiquette, "5, 8, fill, default");

		
		JLabel lbl_montant = new JLabel("Montant :");
		lbl_montant.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lbl_montant.setHorizontalAlignment(SwingConstants.RIGHT);
		contentPane.add(lbl_montant, "2, 10");
		

		tf_montant = new JTextField();
		tf_montant.setColumns(10);
		contentPane.add(tf_montant, "5, 10, fill, default");

		
		lbl_euro = new JLabel("\u20AC");
		lbl_euro.setHorizontalAlignment(SwingConstants.RIGHT);
		contentPane.add(lbl_euro, "6, 10");
		
		JLabel lbl_date = new JLabel("Date :");
		lbl_date.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lbl_date.setHorizontalAlignment(SwingConstants.RIGHT);
		contentPane.add(lbl_date, "2, 12");
		
		dateChooser = new JDateChooser();
		contentPane.add(dateChooser, "5, 12, 2, 1, fill, fill");
		

		 btn_ok = new JButton("OK");
		contentPane.add(btn_ok, "8, 16");

	}
	
	public boolean isDebit_credit() {
		return debit_credit;
	}

	public void setDebit_credit(boolean set_debit_credit) {
		debit_credit = set_debit_credit;
	}

	public double getMontant() {
		return montant;
	}

	public void setMontant(int set_montant) {
		montant = set_montant;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String set_description) {
		description = set_description;
	}

	public Date getDate_operation() {
		return date_operation;
	}

	public void setDate_operation(Date set_date_operation) {
		date_operation = set_date_operation;
	}

	public String getEtiquette() {
		return etiquette;
	}

	public void setEtiquette(String set_etiquette) {
		etiquette = set_etiquette;
	}
	
	public JButton getButton() {
		return btn_ok;
	}

	/**
	 * M�thode retournant un objet de type Operation cr�� � partir des donn�es fournies par l'utilisateur
	 * @return Operation op�ration cr�� � l'aide de la fen�tre
	 */
	public Operation getOperation()
	{	
		// V�rifier si l'op�ration est un d�bit ou un cr�dit 
		if(!tf_montant.getText().isEmpty())
			montant = Double.parseDouble(tf_montant.getText());
		else
			montant = 0;
		
		if (montant < 0)
		{
			debit_credit = false;
			montant = - montant;
		}
		else
		{
			debit_credit = true;
		}
		
		description = tf_libelle.getText();
		
		// Tester si la date est null
		if(dateChooser.getDate() == null)
		{
			date_operation = new Date();
		}
		else
		{
			// Sinon utiliser la date fournie par l'utilisateur
			date_operation = dateChooser.getDate();
		}
		
		
		
		etiquette = (String) cb_etiquette.getSelectedItem();
		Operation retour = new Operation(date_operation, montant, debit_credit, description, etiquette);
		return retour;
	}
	
}
