import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.RowSpec;
import com.jgoodies.forms.factories.FormFactory;

import javax.swing.JLabel;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import javax.swing.JTextField;

/**
 * Class permettant d'afficher la fen�tre de g�n�ration de rapport
 * @author Guillaume BITAUD
 *
 */
public class GenererRapportUI extends JFrame {

	// A utiliser dans le cas d'une s�rialisation de cette class
	private static final long serialVersionUID = -4198801868053247672L;
	
	private JPanel contentPane;
	private JTextField txtNomdufichierpdf;
	private JButton btnGnrer;
	private boolean etat_generation_rapport;

	/**
	 * Test Unitaires.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Date date = new Date(); // your date
				    
					Calendar cal = Calendar.getInstance();
				    cal.setTime(date);
				    
				    cal.getTime();
				    int year = cal.get(Calendar.YEAR);
				    
					Compte cpt = new Compte("Test du graphique Histogramme", 0, "Compte test");
					double credit = 0;
					double debit = 0;
					for(int i = 0; i<12;i++)
					{
						double credit_tmp = Math.random()*1000; 
						double debit_tmp = Math.random()*1000;
						
						credit += credit_tmp;
						debit += debit_tmp;
						
						cal.set(2015, i, 1);
						cpt.Add_Opperation(new Operation(cal.getTime(),debit_tmp , false, "Test", "Boutique"));
						cpt.Add_Opperation(new Operation(cal.getTime(),50 , false, "Test", "Restaurant"));
						cpt.Add_Opperation(new Operation(cal.getTime(),30 , false, "Test", "Internet"));
						cpt.Add_Opperation(new Operation(cal.getTime(),15 , false, "Test", "Coiffeur"));
						cpt.Add_Opperation(new Operation(cal.getTime(),100 , true, "Test", "Vente TV"));
						cpt.Add_Opperation(new Operation(cal.getTime(),120 , true, "Test", "Vente T�l�phone"));
						cpt.Add_Opperation(new Operation(cal.getTime(),credit_tmp , true, "Test", "Vente service"));
						
					}
					ArrayList<Compte> liste_comptes = new ArrayList<Compte>();
					liste_comptes.add(cpt);
					liste_comptes.add(new Compte("Test du graphique Histogrammedddd", 0, "Compte test2"));
					GenererRapportUI frame = new GenererRapportUI(liste_comptes);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 * @param liste_comptes Liste des comptes disponibles pour g�n�rer un rapport
	 */
	public GenererRapportUI(ArrayList<Compte> liste_comptes) {
		setTitle("Edition d'un rapport");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new FormLayout(new ColumnSpec[] {
				FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("default:grow"),
				FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC,},
			new RowSpec[] {
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,}));
		
		JLabel lblEditionDunRapport = new JLabel("Edition d'un rapport");
		contentPane.add(lblEditionDunRapport, "4, 2, 3, 1, center, default");
		
		JLabel lblNomDuFichier = new JLabel("S\u00E9lection du compte");
		contentPane.add(lblNomDuFichier, "4, 4, right, top");
		
		JComboBox<Compte> cb_liste_comptes = new JComboBox<Compte>();
		
		// Remplir la ComboBox avec les comptes disponibles
		for(int i = 0; i< liste_comptes.size(); i++)
		{
			cb_liste_comptes.addItem(liste_comptes.get(i));
		}
		
		cb_liste_comptes.setRenderer(new ComboBoxRender());
		contentPane.add(cb_liste_comptes, "6, 4, fill, default");
		
		JLabel lblTypeRapport = new JLabel("Type de rapport");
		contentPane.add(lblTypeRapport, "4, 6, right, default");
		
		JComboBox<String> cb_type_rapport = new JComboBox<String>();
		cb_type_rapport.setModel(new DefaultComboBoxModel<String>(new String []{"Rapport complet"}));
		
		contentPane.add(cb_type_rapport, "6, 6, fill, default");
		
		JLabel lblNomDuFichier_1 = new JLabel("Nom du fichier");
		contentPane.add(lblNomDuFichier_1, "4, 8, right, default");
		
		txtNomdufichierpdf = new JTextField();
		txtNomdufichierpdf.setText("nom_du_fichier");
		contentPane.add(txtNomdufichierpdf, "6, 8, fill, default");
		txtNomdufichierpdf.setColumns(10);
		
		JLabel lblpdf = new JLabel(".pdf");
		contentPane.add(lblpdf, "8, 8");
		
		JLabel lblOptions = new JLabel("Options :");
		contentPane.add(lblOptions, "4, 10");
		
		JCheckBox checkb_grahique3D = new JCheckBox("graphique 3D");
		contentPane.add(checkb_grahique3D, "6, 10");
		
		
		JLabel lblEtat = new JLabel("Etat:");
		contentPane.add(lblEtat, "6, 16");
		
		btnGnrer = new JButton("G\u00E9n\u00E9rer");
		btnGnrer.addActionListener(new ActionListener() {
			
			// Lorsque le bouton G�n�rer est cliqu�
			// G�n�rer un nouveau rapport en fonction des param�tres indiqu�s par l'utilisateur
			public void actionPerformed(ActionEvent arg0) {
				
				Rapport rapport = new Rapport(txtNomdufichierpdf.getText(), "Titre du rapport","Rapport de compte", "compte", "Auteur", (Compte)cb_liste_comptes.getSelectedItem() );
				if(!rapport.GenererRapportPDF(0, checkb_grahique3D.isSelected()))
				{
					// Si la g�n�ration d'un rapport est un succ�s, le signaler � l'utilisateur
					lblEtat.setText("Etat : Le rapport a �t� �dit�");
					etat_generation_rapport = true;
				}
				else
				{
					// Sinon, si c'est un �chec, afficher une erreur
					lblEtat.setText("Etat : Erreur durant la g�n�ration du rapport");
					etat_generation_rapport = false;
				}
			}
		});
		contentPane.add(btnGnrer, "6, 14");
		
	}

	public JButton getBtnGnrer() {
		return btnGnrer;
	}

	public boolean isEtat_generation_rapportOK() {
		return etat_generation_rapport;
	}

	public void setEtat_generation_rapport(boolean etat_generation_rapport) {
		this.etat_generation_rapport = etat_generation_rapport;
	}
}
