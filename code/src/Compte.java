
/**
 * Fichier Compte.java
 * @author Pierre LE POTTIER
 */

import java.io.Serializable;
import java.util.Date;
import java.util.LinkedList;


/**
 * Class Compte
 * @author Pierre LE POTTIER
 *
 */
public class Compte implements Serializable{

	// A utiliser dans le cas d'une s�rialisation de cette class
	private static final long serialVersionUID = 7636986706786552768L;
	
	protected String description; //Variable d'instance description
	protected double montant_dispo; //Variable d'instance montant_disponible
	protected LinkedList<Operation> liste_operation;
	protected String nom;
	protected Date date_compte ;
	protected double solde_minimum;
	protected double solde_init;

	
	/**
	 * Constructeur permettant de d�finir les attributs nom, description et montant_dispo
	 * @param description_def description du compte
	 * @param montant_dispo_def montant disponible sur le compte
	 */
	public Compte(String description_def, double montant_dispo_def, String name){
		description 	= description_def;
		solde_init = montant_dispo_def;
		montant_dispo 	= solde_init;
		liste_operation = new LinkedList<Operation>();
		nom 			= name;
		solde_init = 0;
				
	}
	
	/**
	 * Constructeur plus �labor� de la class Compte
	 * @param name Nom du compte
	 * @param description_def Description du Compte
	 * @param montant_dispo_def Montant disponible
	 * @param date_compte_def Date de cr�ation du compte
	 * @param solde_mini Solde minimum admissible
	 */
	public Compte(String name, String description_def, double montant_dispo_def, Date date_compte_def, double solde_mini)
	{
		description = description_def;
		solde_init = montant_dispo_def;
		montant_dispo 	= solde_init;
		date_compte = date_compte_def;
		solde_minimum = solde_mini;
		liste_operation = new LinkedList<Operation>();
		nom = name;
	}


	/**
	 * Tests Unitaire
	 * @param args
	 */
	public static void main(String[] args) {
		
		Compte compte = new Compte("Pierre LE POTTIER", 500, "Compte1");
		System.out.println(compte);
		
		Date date = new Date();
		Operation opp1 = new Operation(date, 500,false, "achat ordi", "Shopping");
		Operation opp2 = new Operation(date, -500,false, "achat ordi", "Shopping");
		
		compte.Add_Opperation(opp1);
		System.out.println(compte.getMontant_dispo() + "�");
		
		compte.Add_Opperation(opp2);
		System.out.println(compte.getMontant_dispo() + "�");

	}
	
	/**
	 * Ajoute une op�ration � la liste des op�rations
	 * Lorsqu'une op�ration est ajout�e, le solde est automatiquement recalcul�
	 * @param opp Op�ration � ajouter
	 */
	public void Add_Opperation(Operation opp)
	{
		System.out.println("Add_operation dans Compte : " + opp);
		liste_operation.add(opp);
		Refresh_solde(opp);
		
	}
	
	/**
	 * Recalculer le solde diponible pour le compte
	 * @param opp
	 */
	public double Refresh_solde(Operation opp)
	{
		// Si c'est un cr�dit
		if((opp.isDebit_credit() == true))
		{
			montant_dispo = montant_dispo + opp.getMontant();
		}
		// Si c'est un d�bit
		else
		{
			montant_dispo = montant_dispo - opp.getMontant();
		}
		
		return montant_dispo;		
	}
	
	/**
	 * Recalculer le solde diponible pour le compte
	 */
	public double Refresh_solde()
	{
		montant_dispo = solde_init;
		for(int i = 0; i< liste_operation.size();i++)
		{
			if(liste_operation.get(i).isDebit_credit())
				montant_dispo += liste_operation.get(i).getMontant();
			else
				montant_dispo -= liste_operation.get(i).getMontant();
			
			
		}
		return montant_dispo;
	}
	
	/**
	 * Impl�mentation de la m�thode toString()
	 */
	public String toString()
	{
		return "Vous avez cr�� le compte : " + nom + " avec un montant disponible de " + montant_dispo + " � ";
	}
	
	/**
	 * Obtenir la description du compte
	 * @return
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Mettre � jour la description du compte
	 * @return
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	
	/**
	 * Obtenir le montant disponible sur le compte
	 * @return
	 */
	public double getMontant_dispo() {
		return montant_dispo;
	}

	/**
	 * Mettre � jour le montant disponible sur le compte
	 * @return
	 */
	public void setMontant_dispo(Float montant_dispo) {
		this.montant_dispo = montant_dispo;
	}
	
	/**
	 * Retourne la liste des op�rations du compte
	 * @return LinkedList<Operation> Liste des op�rations r�alis�es sur le compte.
	 */
	public LinkedList<Operation> getListeOperations(){
		return liste_operation; 
	}


	public String getName() {
		return nom;
	}


	public void setName(String nom) {
		this.nom = nom;
	}
	
	public Date getDate_compte() {
		return date_compte;
	}

	public void setDate_compte(Date date_compte) {
		this.date_compte = date_compte;
	}

	public double getSolde_minimum() {
		return solde_minimum;
	}

	public void setSolde_minimum(double solde_minimum) {
		this.solde_minimum = solde_minimum;
	}

	public double getSolde_init() {
		return solde_init;
	}

	public void setSolde_init(double solde_init) {
		this.solde_init = solde_init;
	}

}
