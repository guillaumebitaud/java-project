import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedList;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.general.DefaultPieDataset;

/**
 * Class permettant de g�n�rer diff�rents types de graphiques � l'aide de la biblioth�que JFreeChart
 * @author Guillaume BITAUD
 *
 */
public class Graphique {
	
	private JFreeChart data;
	
	/**
	 * Constructeur par d�faut
	 */
	public Graphique(int type_graphique, Compte compte, boolean option){
			
		// Test du type de graphique � afficher
		switch(type_graphique)
		{
			case 0:
				data = PreparerDiagrammeGains(compte, option);	// Diagramme des gains
				break;
			case 1:
				data = PreparerDiagrammeDebits(compte, option);	// Diagramme des pertes
				break;
			case 2:
				data = PreparerHistogramme(compte, option);	// Histogramme 2D
				break;
			default:
				break;
		}
	}
	
	/**
	 * M�thode permettant de pr�parer un diagramme de toutes les op�rations cr�ditrices
	 * @param cpt Compte � partir duquel r�cup�rer les op�rations
	 * @param opt false : graphique 2D 
	 * 			  true	: graphique 3D
	 * @return Retourne un graphique 
	 */
	private JFreeChart PreparerDiagrammeGains(Compte cpt, boolean opt){
		
		// Liste d'op�ration
		LinkedList<Operation> liste_operations = cpt.getListeOperations();
		
		// Table de hashage permettant de m�moriser les �tiquettes
		Hashtable<String,Double> hashtable = new Hashtable<String,Double>();
		
		// Liste permettant de calculer les pourcentages
		ArrayList<ListeGraphique> liste_camembert = new ArrayList<ListeGraphique>();
		
		// Camembert 2D ou 3D
		JFreeChart chart;
		
		// Instanciation de data en object de type camembert
		DefaultPieDataset data_camembert = new DefaultPieDataset();
		
		// Valeur de toutes op�rations cr�ditrices (pour pourcentage)
		double valeur_max = 0.0;
		
		// Valeur temporaire 
		double valeur_tmp = 0.0;
		
		// Parcourir toutes les op�rations
		for(int i = 0; i<liste_operations.size();i++)
		{
			// Si c'est un cr�dit
			if(liste_operations.get(i).isDebit_credit())
			{
				if(hashtable.containsKey(liste_operations.get(i).getEtiquette()))
				{
					valeur_tmp = hashtable.get(liste_operations.get(i).getEtiquette());
					valeur_tmp += liste_operations.get(i).getMontant();
					hashtable.put(liste_operations.get(i).getEtiquette(), valeur_tmp);
				}
				else
				{
					hashtable.put(liste_operations.get(i).getEtiquette(), liste_operations.get(i).getMontant());
				}
				valeur_max += liste_operations.get(i).getMontant(); 
			}
		}
		
		// Calcul des pourcentages
		Iterator<String> it;
		it=hashtable.keySet().iterator();
				
		// Calculer le poucentage de chaque �tiquette par rapport au montant total.
		while(it.hasNext())
		{
		   String key  = it.next();
		   Double value = hashtable.get(key);
		   Double pourcent = (value*100)/(valeur_max);
		   liste_camembert.add(new ListeGraphique(key, pourcent));
		}
		
		// Associer chaquette �tiquette � une entr�e de data
		for(int i=0; i< liste_camembert.size();i++)
		{
			data_camembert.setValue(liste_camembert.get(i).getEtiquette()+"\n "+ (int)liste_camembert.get(i).getValeur()+ "%", liste_camembert.get(i).getValeur());
		}
			
		// Si l'option opt est �gale � true, le camembert doit �tre en 3D
		if(opt == true)
		{
			chart = ChartFactory.createPieChart3D("Diagramme des Cr�dits",data_camembert,true,true,true);
		}
		// Sinon, le camembert est en 2D
		else
		{
			chart = ChartFactory.createPieChart("Diagramme des Cr�dits",data_camembert,true,true,true);
		}
		
		return chart;
	}
	
	/**
	 * M�thode permettant de pr�parer un diagramme de toutes les op�rations d�bitrices
	 * @param cpt Compte � partir duquel r�cup�rer les op�rations
	 * @param opt false : graphique 2D 
	 * 			  true	: graphique 3D
	 * @return Retourne un graphique JFreeChart des op�rations d�bitrices
	 */
	private JFreeChart PreparerDiagrammeDebits(Compte cpt, boolean opt){
		
		// Liste d'op�ration
		LinkedList<Operation> liste_operations = cpt.getListeOperations();
		
		// Table de hashage permettant de m�moriser les �tiquettes
		Hashtable<String,Double> hashtable = new Hashtable<String,Double>();
		
		// Liste permettant de calculer les pourcentages
		ArrayList<ListeGraphique> liste_camembert = new ArrayList<ListeGraphique>();
		
		// Camembert 2D ou 3D
		JFreeChart chart;
		
		// Instanciation de data en object de type camembert
		DefaultPieDataset data_camembert = new DefaultPieDataset();
		
		// Valeur de toutes op�rations cr�ditrices (pour pourcentage)
		double valeur_max = 0.0;
		
		// Valeur temporaire 
		double valeur_tmp = 0.0;
		
		for(int i = 0; i<liste_operations.size();i++)
		{
			// Si c'est un d�bit
			if(liste_operations.get(i).isDebit_credit() == false)
			{
				// Si l'�tiquette de l'op�ration existe d�j� dans la table de hachage
				if(hashtable.containsKey(liste_operations.get(i).getEtiquette()))
				{
					valeur_tmp = hashtable.get(liste_operations.get(i).getEtiquette());
					valeur_tmp += liste_operations.get(i).getMontant();
					hashtable.put(liste_operations.get(i).getEtiquette(), valeur_tmp);
				}
				// Sinon, il faut la cr�er
				else
				{
					valeur_tmp = liste_operations.get(i).getMontant();
					hashtable.put(liste_operations.get(i).getEtiquette(), valeur_tmp);
				}
				valeur_max += liste_operations.get(i).getMontant(); 
			}
		}
		
		// Calcul des pourcentages
		Iterator<String> it;
		it=hashtable.keySet().iterator();
				
		// Calculer le poucentage de chaque �tiquette par rapport au montant total.
		while(it.hasNext())
		{
		   String key  = it.next();
		   Double value = hashtable.get(key);
		   Double pourcent = (value*100)/(valeur_max);
		   liste_camembert.add(new ListeGraphique(key, pourcent));
		}
		
		// Associer chaque �tiquette � une entr�e de data
		for(int i=0; i< liste_camembert.size();i++)
		{
			data_camembert.setValue(liste_camembert.get(i).getEtiquette()+"\n "+ (int)liste_camembert.get(i).getValeur()+ "%", liste_camembert.get(i).getValeur());
		}
			
		// Si l'option opt est �gale � true, le camembert doit �tre en 3D
		if(opt == true)
		{
			chart = ChartFactory.createPieChart3D("Diagramme des D�bits",data_camembert,true,true,true);
		}
		// Sinon, le camembert est en 2D
		else
		{
			chart = ChartFactory.createPieChart("Diagramme des D�bits",data_camembert,true,true,true);
		}
		
		return chart;
	}
	
	
	/**
	 * M�thode permettant de pr�parer un histogramme
	 * @param cpt Compte � partir duquel extraire les donn�es
	 * @param opt option : 	- true : Histogramme 3D
	 * 						- false: Histogramme 2D
	 * @return
	 */
	private JFreeChart PreparerHistogramme(Compte cpt, boolean opt){
		// Histogramme 2D ou 3D
		JFreeChart chart;
		
		// Liste d'op�ration
		LinkedList<Operation> liste_operations = cpt.getListeOperations();
				 
		// Instanciation de data en object de type camembert
		DefaultCategoryDataset data_histogramme = new DefaultCategoryDataset();
		
		// Tableau permettant d'associer un num�ro de mois � la String correspondante
		String mois[]={"Janvier", "F�vrier", "Mars", "Avril", "Mai", "Juin", "Juillet", "Aout", "Septembre", "Octobre", "Novembre", "Decembre"};
		
		// Table de hashage permettant de m�moriser les d�bits pour chaque mois
		Hashtable<String,Double> hashtable_credit = new Hashtable<String,Double>();
		
		// Table de hashage permettant de m�moriser les cr�dits pour chaque mois
		Hashtable<String,Double> hashtable_debit = new Hashtable<String,Double>();
				
		
		// R�cup�ration de la Date du jour
		Date date = new Date(); 
	    Calendar cal = Calendar.getInstance();
	    cal.setTime(date);
	    int year = cal.get(Calendar.YEAR);
	    
	    Calendar date_op = Calendar.getInstance();
	    
	    for(int i=0; i<12;i++)
	    {
	    	hashtable_credit.put(mois[i], 0.0);
	    	hashtable_debit.put(mois[i], 0.0);
	    	
	    }
	    // Parcourir toutes les op�rations du compte
		for(int i=0; i<liste_operations.size();i++)
		{
			date_op.setTime(liste_operations.get(i).getDate_transaction());
			
			// Ne s�lectionner que les op�rations r�alis�es pendant l'ann�e courante pour le graphique
			if(date_op.get(Calendar.YEAR) == year)
			{
				// Si c'est un cr�dit
				if(liste_operations.get(i).isDebit_credit() == true)
				{
					Double valeur_tmp = hashtable_credit.get(mois[date_op.get(Calendar.MONTH)]);	 
					valeur_tmp += liste_operations.get(i).getMontant();
					hashtable_credit.put(mois[date_op.get(Calendar.MONTH)], valeur_tmp);
				}
				// Sinon, si c'est un d�bit
				else
				{
					Double valeur_tmp = hashtable_debit.get(mois[date_op.get(Calendar.MONTH)]);
					valeur_tmp += liste_operations.get(i).getMontant();
					hashtable_debit.put(mois[date_op.get(Calendar.MONTH)], valeur_tmp);
				}
			}
		}
		
		// Parcourir les cr�dits et les ajouter � data_histogramme.
		for(int i=0; i<12;i++)
		{
		   data_histogramme.addValue(hashtable_credit.get(mois[i]),"Cr�dit" , mois[i]);
		   data_histogramme.addValue(hashtable_debit.get(mois[i]),"D�bit" , mois[i]);
		   data_histogramme.addValue(hashtable_credit.get(mois[i]) - hashtable_debit.get(mois[i]),"Solde" , mois[i]);
		}
		
		
		// Si l'option opt est �gale � true, l'histogramme doit �tre en 3D
		if(opt == true)
		{
			chart = ChartFactory.createBarChart3D("Evolution du compte", "", "�", data_histogramme);
		}
		// Sinon, l'histogramme est en 2D
		else
		{
			chart = ChartFactory.createBarChart("Evolution du compte", "", "�", data_histogramme);
		}
		
		return chart;
		
	}
	
	/**
	 * Methode permettant d'exporter le graphique cr�� dans un fichier au format png.
	 * @param nom Nom du fichier de sortie
	 * @param taille_x Taille de l'image en abscisse
	 * @param taille_y Taille de l'image en ordonn�e
	 * @return Retourne un objet de type File pointant sur le fichier.png cr��
	 */
	public File ExportImage(String nom, int taille_x, int taille_y)
	{
		File fichier = new File(nom);
		try {
			// Sauvegarde du graphique dans le fichier sous forme d'image .png
			ChartUtilities.saveChartAsPNG(fichier, data, taille_x, taille_y);
		} 
		catch (IOException e) {
			e.printStackTrace();
		}
		
		return fichier;
	}
	
	
	/**
	 * Tests Unitaires
	 * @param args
	 */
	public static void main(String[] args) {
		
		/**
		 * Test permettant de v�rifier l'installation de la biblioth�que JFreeChart
		 * Etat du test : OK
		 */
		/*// Cr�ation d'une graphique de type camembert
		DefaultPieDataset dataset = new DefaultPieDataset();
		dataset.setValue("Category 1", 43.2);
		dataset.setValue("Category 2", 27.9);
		dataset.setValue("Category 3", 79.5);
		
		// Cr�ation du graphique prenant en param�tre les donn�es fournies par dataset
		JFreeChart chart = ChartFactory.createPieChart("Test graphique camembert",	dataset,true,true,false	);
		
		// Cr�ation d'un fichier pour contenir l'image en sortie
		File fichier = new File("image.png");
		
		try {
			// Sauvegarde du graphique dans le fichier sous forme d'image .png
			ChartUtilities.saveChartAsPNG(fichier, chart, 400, 250);
		} 
		catch (IOException e) {
			e.printStackTrace();
		}
		*/
		/**
		 * Test de la fonction de la methode PreparerCamembert
		 * Etat du test : OK
		 */
		/*Compte cpt = new Compte("Test du graphique camembert", 0);
		cpt.Add_Opperation(new Operation(new Date(), 100, true, "Test", "boutique"));
		cpt.Add_Opperation(new Operation(new Date(), 200, true, "Test", "TV"));
		cpt.Add_Opperation(new Operation(new Date(), 300, true, "Test", "boutique"));
		cpt.Add_Opperation(new Operation(new Date(), 400, true, "Test", "TV"));
		cpt.Add_Opperation(new Operation(new Date(), 500, true, "Test", "Internet"));
		cpt.Add_Opperation(new Operation(new Date(), 600, true, "Test", "Internet"));
		cpt.Add_Opperation(new Operation(new Date(), 700, true, "Test", "boutique"));
		cpt.Add_Opperation(new Operation(new Date(), 800, true, "Test", "EDF"));
		cpt.Add_Opperation(new Operation(new Date(), 850, true, "Test", "EDF"));
		System.out.println(cpt);
		
		Graphique test_camembert = new Graphique(1, cpt, false);
		*/
		
		/**
		 * Test de la fonction de la methode PreparerHistogramme
		 * Etat du test : OK
		 */
		/*Date date = new Date(); // your date
	    
		Calendar cal = Calendar.getInstance();
	    cal.setTime(date);
	    
	    cal.getTime();
	    int year = cal.get(Calendar.YEAR);
	    
		Compte cpt = new Compte("Test du graphique Histogramme", 0, "Compte test");
		double credit = 0;
		double debit = 0;
		for(int i = 0; i<12;i++)
		{
			double credit_tmp = Math.random()*1000; 
			double debit_tmp = Math.random()*1000;
			
			credit += credit_tmp;
			debit += debit_tmp;
			
			cal.set(2015, i, 1);
			cpt.Add_Opperation(new Operation(cal.getTime(),credit_tmp , true, "Test", "boutique"));
			cpt.Add_Opperation(new Operation(cal.getTime(),debit_tmp , false, "Test", "TV"));
		}
		System.out.println("Cr�dit : "+credit);
		System.out.println("D�bit : "+debit);
		System.out.println("Solde du compte : "+(credit-debit));
		
		System.out.println(cpt);
		
		Graphique test_histogramme = new Graphique(0, cpt, false);
		test_histogramme.ExportImage("Camembert2D.png", 800,400);
		Graphique test_histogramme1 = new Graphique(1, cpt, false);
		test_histogramme1.ExportImage("Camembert3D.png", 800,400);
		Graphique test_histogramme2 = new Graphique(2, cpt, false);
		test_histogramme2.ExportImage("Histogramme2D.png", 800,400);
		Graphique test_histogramme3 = new Graphique(3, cpt, false);
		test_histogramme3.ExportImage("Histogramme3D.png", 800,400);
		*/
		/**
		 * Test de la methode PreparerDiagrammeGain
		 * Etat du test : OK
		 */
		/*Date date = new Date(); // your date
	    
		Calendar cal = Calendar.getInstance();
	    cal.setTime(date);
	    
	    cal.getTime();
	    int year = cal.get(Calendar.YEAR);
	    
		Compte cpt = new Compte("Test du Diagramme", 0, "Compte test");
		double credit = 0;
		double debit = 0;
		for(int i = 0; i<12;i++)
		{
			double credit_tmp = Math.random()*1000; 
			double debit_tmp = Math.random()*1000;
			
			credit += credit_tmp;
			debit += debit_tmp;
			
			cal.set(2015, i, 1);
			cpt.Add_Opperation(new Operation(cal.getTime(),credit_tmp , true, "Test", "Vente produits"));
			cpt.Add_Opperation(new Operation(cal.getTime(),credit_tmp , true, "Test", "Vente Services"));
			cpt.Add_Opperation(new Operation(cal.getTime(),debit_tmp , false, "Test", "TV"));
			cpt.Add_Opperation(new Operation(cal.getTime(),debit_tmp , false, "Test", "Internet"));
		}
		System.out.println("Cr�dit : "+credit);
		System.out.println("D�bit : "+debit);
		System.out.println("Solde du compte : "+(credit-debit));
		
		System.out.println(cpt);
		
		Graphique test_diagramme = new Graphique(0, cpt, false);
		Graphique test_diagramme1 = new Graphique(1, cpt, false);
		Graphique test_diagramme2 = new Graphique(2, cpt, false);
		Graphique test_diagramme3 = new Graphique(3, cpt, false);
		Graphique test_diagramme4 = new Graphique(4, cpt, false);
		Graphique test_diagramme5 = new Graphique(5, cpt, false);
		
		test_diagramme.ExportImage("DiagrammeCredit2D.png", 800,400);
		test_diagramme1.ExportImage("DiagrammeCredit3D.png", 800,400);
		test_diagramme2.ExportImage("DiagrammeDebit2D.png", 800,400);
		test_diagramme3.ExportImage("DiagrammeCredit3D.png", 800,400);
		test_diagramme4.ExportImage("Histogramme2D.png", 800,400);
		test_diagramme5.ExportImage("Histogramme3D.png", 800,400);*/
	}
	
	/**
	 * Class interne permettant de cr�er des Listes pour les graphiques
	 * @author Guillaume BITAUD
	 *
	 */
	public class ListeGraphique{
		private String etiquette;
		private double valeur;
		private String nom_periode;	
		
		/**
		 * Constructeur par defaut
		 */
		public ListeGraphique(){
			
		}
		
		/**
		 * Constructeur d'une liste d'un graphique en camembert.
		 * @param etiquette_def Etiquette de donn�e.
		 * @param val Valeur de la liste de donn�es. 
		 */
		public ListeGraphique(String etiquette_def, double val){
			etiquette = new String(etiquette_def);
			valeur = val;
		}
		
		// Getters et Setters
		 
		public String getEtiquette() {
			return etiquette;
		}
		
		public String getMois(){
			return etiquette;
		}

		public void setEtiquette(String etiquette) {
			this.etiquette = etiquette;
		}

		public double getValeur() {
			return valeur;
		}

		public void setValeur(double valeur) {
			this.valeur = valeur;
		}

		public String getNom_periode() {
			return nom_periode;
		}

		public void setNom_periode(String nom_periode) {
			this.nom_periode = nom_periode;
		}
	}
}

