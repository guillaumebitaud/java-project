import java.io.Serializable;
import java.util.Date;

/**
 * 
 * @author Pierre LE POTTIER
 * Class permettant de d�crire une op�ration bancaire.
 */
public class Operation implements Serializable{
	
	// A utiliser dans le cas d'une s�rialisation de cette class
	private static final long serialVersionUID = 4654430431308883937L;
	
	private Date date_transaction;
	private double montant;
	private boolean debit_credit;
	private String description;
	private String etiquette;
	
	/**
	 * Constructeur principal
	 * @param date_trans Date de l'op�ration.
	 * @param mont Montant de la transaction.
	 * @param deb_cred D�bit(true) ou Cr�dit(false) ? 
	 * @param descript Description de l'op�ration bancaire.
	 * @param etiqt Etiquette de l'op�ration.
	 */
	public Operation(Date date_trans, double mont, boolean deb_cred, String descript, String etiqt)
	{
		date_transaction = date_trans;
		montant = mont;
		debit_credit = deb_cred;
		description = descript;
		etiquette = etiqt;
				
	}
			
	

	/**
	 * Test unitaire
	 */
	public static void main(String[] args) {
		Date date = new Date();
		Operation test_op = new Operation(date, 500.0, false, "achat pc", "shopping");
		System.out.println(test_op);
	}
	
	/**
	 * Impl�mentation de la m�thode toString()
	 */
	public String toString()
	{
		return "L'op�ration du : " +date_transaction + " de " + montant + "� " + " debit/cr�dit : " + debit_credit + 
			" a �t� enregistr�e " + description + " dans l'�tiquette " + etiquette; 
	}
	
	
	// getters & setters associ�s
	public Date getDate_transaction() {
		return date_transaction;
	}
	public void setDate_transaction(Date date_transaction) {
		this.date_transaction = date_transaction;
	}

	public boolean isDebit_credit() {
		return debit_credit;
	}
	public void setDebit_credit(boolean debit_credit) {
		this.debit_credit = debit_credit;
	}
	
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}

	public String getEtiquette() {
		return etiquette;
	}
	public void setEtiquette(String etiquette) {
		this.etiquette = etiquette;
	}
	
	public double getMontant() {
		return montant;
	}

	public void setMontant(double montant_def) {
		montant = montant_def;
	}
}
