import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.LinkedList;

import org.hamcrest.core.IsInstanceOf;

import com.itextpdf.text.Anchor;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chapter;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

/**
 * @file Rapport.java
 * @author Guillaume BITAUD
 * @date 13/09/2015
 * @version 0.1a
 */

/**
 * @author Guillaume BITAUD
 * Class permettant de g�n�rer des rapports pdf � l'aide de la biblioth�que iText 
 */
public class Rapport {

	// Attributs
	private Compte cpt;
	private String titre;
	private String description;
	private String sujet;
	private String keywords;
	private String auteur;
	private Date date_creation;
	private String nom_fichier;
	
	private static Font catFont 	= new Font(Font.FontFamily.TIMES_ROMAN, 18,Font.BOLD);
	private static Font redFont 	= new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.NORMAL, BaseColor.RED);
	private static Font subFont 	= new Font(Font.FontFamily.TIMES_ROMAN, 16, Font.BOLD);
	private static Font smallBold 	= new Font(Font.FontFamily.TIMES_ROMAN, 12,Font.BOLD);
	private static Font defaut		= new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.NORMAL, BaseColor.BLACK);
	private static Font debit		= new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.NORMAL, BaseColor.RED);
	private static Font credit		= new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.NORMAL, BaseColor.GREEN);
	
	// M�thodes
	
	/**
	 * Constructeur par d�faut
	 */
	public Rapport(){
		
	}
	
	/**
	 * Constructeur d'un rapport sur le mois sp�cifi� en param�tre.
	 * @param mois Mois sur lequel porte le Rapport.
	 * @param annee Annee sur laquelle porte le Rapport.
	 */
	public Rapport(String mois, int annee){
		
	}
	
	/**
	 * Constructeur 
	 * @param def_nom_fichier Nom du fichier PDF
	 * @param def_titre Titre du fichier PDF
	 * @param def_sujet Sujet du fichier PDF
	 * @param def_keywords Mots-cl�s du fichier PDF
	 * @param def_auteur Auteur du fichier PDF
	 * @param def_cpt Compte � partir duquel g�n�rer un rapport
	 */
	public Rapport(String def_nom_fichier, String def_titre, String def_sujet, String def_keywords, String def_auteur, Compte def_cpt){
		nom_fichier = def_nom_fichier;
		titre		= def_titre;
		sujet		= def_sujet;
		keywords	= def_keywords;
		auteur		= def_auteur;
		cpt 		= def_cpt;
	}
	
	/**
	 *  Fonction permettant de g�n�rer un rapport au format pdf.
	 *  @param type Type du rapport pdf :
	 * @return Retourne true si le PDF a �t� g�n�r�. Sinon, retourne false.
	 */
	public boolean GenererRapportPDF(int type, boolean graphique3D){
		boolean retour = false;
		String chemin_fichier = "rapports/"+nom_fichier+".pdf";
		
		// Tester si le dossier de sauvegarde des rapports existe
		
		File dossier_rapports = new File("rapports");
		// Tester si le dossier sauvegarde existe
		if(!(dossier_rapports.exists()) || !(dossier_rapports.isDirectory()))
		{
			// Cr�er le dossier rapports si besoin
			dossier_rapports.mkdir();
		}
		
		
		try{
		 Document document = new Document();
	      PdfWriter.getInstance(document, new FileOutputStream(chemin_fichier));
	      document.open();
	      AjouterMetaData(document);
	      AjouterTitre(document);
	      AjouterContenu(document, type, graphique3D);
	      document.close();
	      retour = true;
	    } catch (Exception e) {
	      e.printStackTrace();
	    }		
		return retour;
	}
	
	/**
	 * Cette fonction ajoute des Meta Data au document pdf. Ceux-ci sont visible sous Adobe Reader (Fichier->Proprietes)
	 * @param document Document auquel les metaData doivent etre ajoutees
	 */
	private void AjouterMetaData(Document document) {
	    document.addTitle(titre);
	    document.addSubject(sujet);
	    document.addKeywords(keywords);
	    document.addAuthor(auteur);
	    document.addCreator(auteur);
	  }
	
	/**
	 * Fonction permettant d'ajouter un titre au rapport PDF
	 * @param document Rapport PDF
	 * @throws DocumentException
	 */
	private void AjouterTitre(Document document) throws DocumentException {
		    // date d'�dition du rapport :
			DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
			
			Paragraph preface = new Paragraph();
		    // Ajouter une ligne vide
		    addEmptyLine(preface, 1);
		    // Header
		    preface.add(new Paragraph("Rapport complet de l'�tat du compte "+ cpt.getName(), catFont));
		    addEmptyLine(preface, 4);
		    preface.add(new Paragraph("Description du document ", smallBold));
		    
		    preface.add(new Paragraph("Ce rapport pr�sente l'�tat de votre compte "+cpt.getName()+" � la date du "+df.format(new Date())+". "+
		    		"Il a �t� g�n�r� par " + System.getProperty("user.name")+".", defaut));
		    
		    addEmptyLine(preface, 2);  
		    preface.add(new Paragraph("Quelques informations sur votre compte : "+ cpt.getName(), smallBold));
		    preface.add(new Paragraph("Votre compte "+cpt.getName()+" d'un montant initial de "+cpt.getSolde_init()+" poss�de actuellement un solde de "+cpt.getMontant_dispo()+
		    		" �.", defaut));
		    if(cpt instanceof Compte_Epargne)
		    {
		    	if(((Compte_Epargne) cpt).getInteret() > 0)
		    	{
		    		preface.add(new Paragraph("Le taux d'int�r�t sur ce compte est de  " +((Compte_Epargne) cpt).getInteret()+" %.", defaut));
		    	}
		    }
		    
		    addEmptyLine(preface, 8);

		    document.add(preface);
		    // Commencer une nouvelle page
		    document.newPage();
		  }
	
	/**
	 * Fonction permettant d'ajouter une ligne vide.
	 * @param paragraph
	 * @param number
	 */
	private void addEmptyLine(Paragraph paragraph, int number) {
	    for (int i = 0; i < number; i++) {
	      paragraph.add(new Paragraph(" "));
	    }
	  }
	
	/**
	 * Fonction permettant de d�finir completement le contenu du rapport ainsi que le position des elements dans le document PDF.
	 * @param document
	 * @param type Type du Document 0:
	 * @throws DocumentException
	 */
	private void AjouterContenu(Document document, int type, boolean graphique3D) throws DocumentException {
	    switch(type)
	    {
	    	/** Rapport Complet :
	    	 * 		- Tableau de toutes les op�rations
	    	 * 		- Camembert repr�sentant les d�penses
	    	 * 		- Camembert repr�sentant les gains
	    	 * 		- Histogramme Affichant les d�penses/gains/soldes par mois
	    	 */
		    case 0:
		    	ContenuRapportComplet(document, graphique3D);
		    	break;
		    /** Rapport Op�rations :
		     * 		- Tableau de toutes les op�rations
		     */	
		    case 1:
		    	break;
		    /** Rapport R�sum� graphique :
			 * 		- Camembert repr�sentant les d�penses
			 * 		- Camembert repr�sentant les gains
			 * 		- Histogramme Affichant les d�penses/gains/soldes par mois
			 */			    	
		    case 2:
		    	break;
		    default:
		    	break;
	    }
	}
	
	/**
	 * Methode permettant de g�n�rer le contenu d'un Rapport Complet.
	 * @param graphique3D
	 */
	private void ContenuRapportComplet(Document document, boolean graphique3D)
	{
		// Ajouter un nouveau Chapitre : "Op�rations du compte"
		Anchor anchor = new Anchor("Op�rations du compte", catFont);
	    anchor.setName("Op�rations du compte");
	    // Il s'agit du Chapitre n�1
	    Chapter catPart = new Chapter(new Paragraph(anchor), 1);
	    catPart.add(new Paragraph("Le d�tail de toutes les op�rations r�alis�es sur ce compte est pr�sent� ci-apr�s :"));
	     
	    
	    // Ajout du Chapitre 1 au document
	    try {
			document.add(catPart);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    
	    // Ajout d'un saut de ligne
	    Paragraph empty_line = new Paragraph();
	    addEmptyLine(empty_line, 2);
	    try {
			document.add(empty_line);
		} catch (DocumentException e3) {
			// TODO Auto-generated catch block
			e3.printStackTrace();
		}
	    
	   // Ajouter un tableau de toutes les op�rations du compte
	  //specify column widths
	    float[] columnWidths = {2.5f, 4f, 2.5f, 2f};
	    //create PDF table with the given widths
	    PdfPTable table = new PdfPTable(columnWidths);
	    // Le tableau est centr� et occupe 90 % de la page en largeur
	    table.setWidthPercentage(90f);
	    
	    // Ajouter les ent�tes	
	    insertCell(table, "Date", Element.ALIGN_CENTER, 1, smallBold);
	    insertCell(table, "Description", Element.ALIGN_CENTER, 1, smallBold);
	    insertCell(table, "Etiquette", Element.ALIGN_CENTER, 1, smallBold);
	    insertCell(table, "Montant", Element.ALIGN_CENTER, 1, smallBold);
	    table.setHeaderRows(1);
	        
	    
	    // Trier les op�rations par date :
	    Collections.sort(cpt.getListeOperations(), new Comparator<Operation>() {
	    	   public int compare(Operation o1, Operation o2){
	    	      Date a = o1.getDate_transaction();
	    	      Date b = o2.getDate_transaction();
	    	      if (a.before(b)) 
	    	        return -1;
	    	      else if (a.equals(b)) // it's equals
	    	         return 0;
	    	      else
	    	         return 1;
	    	   }
	    	});

	    // Formater les dates dans le bon format
	    DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
	    // Ins�rer les op�rations :
	    for(int i = 0; i< cpt.getListeOperations().size(); i++)
	    {
	    	
	    	insertCell(table, df.format(cpt.getListeOperations().get(i).getDate_transaction()), Element.ALIGN_RIGHT, 1, defaut);
	    	insertCell(table, cpt.getListeOperations().get(i).getDescription(), Element.ALIGN_RIGHT, 1, defaut);
	    	insertCell(table, cpt.getListeOperations().get(i).getEtiquette() + " ", Element.ALIGN_RIGHT, 1, defaut);
	    	
	    	// SI c'est un d�bit afficher un plus
	    	if(cpt.getListeOperations().get(i).isDebit_credit() == true)
	    	{
	    		insertCell(table,"+ " +  cpt.getListeOperations().get(i).getMontant() + " �", Element.ALIGN_RIGHT, 1, credit);
	    	}
	    	// Sinon, afficher un moins
	    	else
	    	{
	    		insertCell(table, "- " + cpt.getListeOperations().get(i).getMontant() + " �", Element.ALIGN_RIGHT, 1, debit);
	    	}
	    	
	    }
	    
	  // Cr�er une case Total
	    insertCell(table, " Total ", Element.ALIGN_RIGHT, 3, smallBold);
	    insertCell(table, cpt.getMontant_dispo() + " �", Element.ALIGN_RIGHT, 1, smallBold);
	    
	    // Ajouter le tableau � un paragraphe 
	    Paragraph paragraph = new Paragraph();
	    paragraph.add(table);
	    // Ajouter le paragraphe au document
	    try {
			document.add(paragraph);
		} catch (DocumentException e3) {
			// TODO Auto-generated catch block
			e3.printStackTrace();
		}
	    
		
	 // Ajouter un nouveau Chapitre : "R�sum� des d�penses"
	anchor = new Anchor("R�sum� des d�penses", catFont);
    anchor.setName("R�sum� des d�penses");
    // Il s'agit du Chapitre n�2
    catPart = new Chapter(new Paragraph(anchor), 2);
    catPart.add(new Paragraph("Le diagramme suivant r�sume les d�penses par type :"));
    empty_line = new Paragraph();
    addEmptyLine(empty_line, 2);
    try {
		document.add(empty_line);
	} catch (DocumentException e2) {
		// TODO Auto-generated catch block
		e2.printStackTrace();
	}
    try {
		document.add(catPart);
	} catch (DocumentException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
    
    // Ajouter un Diagramme affichant un r�sum� des d�penses
    Image image;
    try {
		image = Image.getInstance(GenererGraphique(1, cpt, graphique3D, 500, 300, "tmp_diag_debits.png"));
		try {
			document.add(image);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	} catch (BadElementException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	} catch (MalformedURLException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	} catch (IOException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
	
	
    
    // Ajouter un nouveau Chapitre : "R�sum� des gains"
	anchor = new Anchor("R�sum� des gains", catFont);
	anchor.setName("R�sum� des gains");
	// Il s'agit du Chapitre n�3
	catPart = new Chapter(new Paragraph(anchor), 3);
	catPart.add(new Paragraph("Le diagramme suivant r�sume les gains par type :"));
	
	try {
		document.add(catPart);
	} catch (DocumentException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
	 // Ajouter un Diagramme affichant un r�sum� des gains
    try {
		image = Image.getInstance(GenererGraphique(0, cpt, graphique3D, 500, 300, "tmp_diag_credits.png"));
		try {
			document.add(image);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	} catch (BadElementException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	} catch (MalformedURLException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	} catch (IOException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
	
	
	// Ajouter un nouveau Chapitre : "R�sum� des gains/d�penses/soldes par mois"
	anchor = new Anchor("R�sum� des gains/d�penses/soldes par mois", catFont);
	anchor.setName("R�sum� des gains/d�penses/soldes par mois");
	// Il s'agit du Chapitre n�4
	catPart = new Chapter(new Paragraph(anchor), 4);
	catPart.add(new Paragraph("L'histogramme suivant r�sume les gains/d�penses/soldes par mois."));
			    
	try {
		document.add(catPart);
		} 
	catch (DocumentException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		}
	
	 // Ajouter un Histogramme affichant un r�sum� des cr�dits/gains/soldes par mois
    try {
		image = Image.getInstance(GenererGraphique(2, cpt, graphique3D, 800, 500, "tmp_hist.png"));
		image.setRotationDegrees(90);
		try {			
			document.add(image);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	} catch (BadElementException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	} catch (MalformedURLException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	} catch (IOException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
	
	}
	
	
	/**
	 * Utiliser la class Graphique pour g�n�rer un graphique sous forme d'image ins�rable dans le rapport.
	 * @param type_graphique    0 : Diagramme Cr�dit 2D
	 * 							1 : Diagramme D�bit 3D
	 * 							2 : Diagramme Cr�dit 2D
	 * 							3 : Diagramme D�bit 3D
	 * 							2 : Histogramme 2D
	 * 							3 : Histogramme 3D
	 * @param compte Compte � partir duquel cr�er un graphique
	 * @param option Option suppl�mentaire
	 * @return Le nom du fichier.png contenant le graphique
	 */
	public String GenererGraphique(int type_graphique, Compte compte, boolean option, int taille_x, int taille_y, String nom_img)
	{
		// Cr�er un nouveau graphique
		Graphique graph = new Graphique(type_graphique, compte, option);
		// Exporter le graphique pr�c�demment cr�� vers un fichier File. 
		return graph.ExportImage(nom_img, taille_x, taille_y).getName();
	}
	
	/**
	 * Ins�rer une cellule dans le tableau
	 * @param table
	 * @param text
	 * @param align
	 * @param colspan
	 * @param font
	 */
	private void insertCell(PdfPTable table, String text, int align, int colspan, Font font){
		   
	// Cr�er une nouvelle cellule avec le texte et la police sp�cifi�e
	  PdfPCell cell = new PdfPCell(new Phrase(text.trim(), font));
	 // Aligner la cellule
	 cell.setHorizontalAlignment(align);
	 cell.setColspan(colspan);
	 //Si il n'y a pas de texte, cr�er une ligne vide
	 if(text.trim().equalsIgnoreCase(""))
	 {
	  cell.setMinimumHeight(10f);
	 }
	 // Ajouter la cellule � la table
	 table.addCell(cell);
		   
		 }
	
	/**
	 * Fonction permettant de d�finir le texte � afficher lors d'un print sur cette class.
	 */
	public String toString(){
		return "Rapport : ";
		
	}
	/**
	 * Tests Unitaires
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		// Cr�ation de l'objet a tester de la class Rapport
		Rapport rapport_test = new Rapport("rapport_test","Titre", "Sujet", "Keyword1 Keyword2", "Guillaume BITAUD", null);
		
		/**
		 * Test de la g�n�ration d'un document PDF minimal
		 * Etat du test : OK
		 */
		rapport_test.GenererRapportPDF(0, false);
		
	}

}
