import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.JTabbedPane;

import java.awt.GridLayout;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;

import javax.swing.JLabel;
import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.io.File;

import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.RowSpec;
import com.jgoodies.forms.factories.FormFactory;

import javax.swing.event.ChangeListener;
import javax.swing.event.ChangeEvent;

/**
 * 
 * @author Guillaume BITAUD
 *
 */
public class MainUI extends JFrame {

	
	private static final long serialVersionUID = -2411888157177895236L;
	private JPanel contentPane;
	private JButton btnAjouterUneOpration;
	private JButton btnSauvegarder;
	private JButton btnGnrerRapport;
	private JButton btnDeleteOperation;
	
	private JTabbedPane tabbedPane_centre;
	
	// JFrame affichant la fenetre d'ajout d'une Op�ration
	private AjouterOperationUI ajouter_operation_ui;
	// JFrame affichant l'outil d'�dition de rapport
	private GenererRapportUI rapportUI;
	// JFrame affichant la fentre d'ajout de compte
	private AjouterCompteUI frame_add_compte;
	
	//JFrame affichant la fenetre des mentions l�gales
	private MentionsLegalesUI mention_legale_ui;
	
	//JFrame affichant la fenetre information du compte
	private InformationCompteUI information_compte_ui;
	
	private JLabel lblMontant;
	private JLabel lbl_etat;
	
	// Utilis� pour contenir toutes les informatins n�cessaires pour afficher le tableau central
	private Tableau tab;
	
	// Correction bug ajout d'un compte la premi�re fois
	private int ancienne_valeur;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainUI frame = new MainUI(null, "test");
					
					// Ajout d'un titre � la fenetre
					frame.setTitle("BL2P : Logiciel de gestion des comptes");
					frame.setVisible(true);
		
					/**
					 * Test Unitaire permettant de tester le fonctionnement de la fonction RecalculerSolde() 
					 */
					System.out.println("Solde Disponible : "+frame.RecalculerSolde());
			
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MainUI(ArrayList<Compte> sauvegarde, String username) {
		setTitle("BL2P : Gestion de comptes");
		tabbedPane_centre = new JTabbedPane(JTabbedPane.TOP);
		
		tab = new Tableau();
		// Si le fichier de sauvegarde est null
		if(sauvegarde != null)
		{
			tab.setListe_comptes(sauvegarde);
			
			// Reconstruire l'interface � partir des donn�es sauvegard�es
			for(int i = 0; i < tab.getListe_comptes().size();i++)
			{
				// Cr�er une nouvelle entr�e au tableau
				tab.getDonnees_tableau().add(new ModeleData());
				
				// Cr�er un nouveau tableau 
				tab.getTableau_operation().add(new JTable(tab.getDonnees_tableau().get(i)));
				
				tab.getListe_scroll().add(new JScrollPane()); 
			
				tab.getListe_scroll().get(i).setViewportView(tab.getTableau_operation().get(i));
				tabbedPane_centre.add(tab.getListe_scroll().get(i), BorderLayout.CENTER);
				tabbedPane_centre.setTitleAt(i, tab.getListe_comptes().get(i).getName());
				System.out.println("Nom du compte "+i+" : " + tab.getListe_comptes().get(i).getName());
				
				
				// Ajouter toutes les op�rations au tableau
				for(int j = 0; j < tab.getListe_comptes().get(i).getListeOperations().size();j++)
				{
					tab.getDonnees_tableau().get(i).addOperationToList(tab.getListe_comptes().get(i).getListeOperations().get(j));
					
					tab.getTableau_operation().get(i).setModel(tab.getDonnees_tableau().get(i));
					tab.getListe_scroll().get(i).setViewportView(tab.getTableau_operation().get(i));
				}
				
			}
			lblMontant = new JLabel(RecalculerSolde()+ " �");
			
		}
		else
		{
			lblMontant = new JLabel("0 �");
		}
		

		
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 894, 450);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JPanel panel_gauche = new JPanel();
		contentPane.add(panel_gauche, BorderLayout.WEST);
		panel_gauche.setLayout(new FormLayout(new ColumnSpec[] {
				ColumnSpec.decode("300px"),},
			new RowSpec[] {
				RowSpec.decode("max(21dlu;default)"),
				RowSpec.decode("max(6dlu;default)"),
				RowSpec.decode("28px"),
				RowSpec.decode("27px"),
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				RowSpec.decode("23px"),
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,}));
		
		JButton btnAddCompte = new JButton("Ajouter un compte");
		btnAddCompte.addActionListener(new ActionListener() {
			// Si le bouton Ajouter un compte est cliqu�
			public void actionPerformed(ActionEvent e) {
				
				frame_add_compte = new AjouterCompteUI();
				frame_add_compte.setVisible(true);
				
				// Si l'utilisateur clique sur le bouton OK de la fen�tre ajouter un compte
				frame_add_compte.getBtnOk().addActionListener(new ActionListener()
				{
					@Override
					public void actionPerformed(ActionEvent arg0) {
						
						// Cr�er une nouvelle entr�e au tableau
						tab.getDonnees_tableau().add(new ModeleData());
						// Cr�er un nouveau tableau 
						tab.getTableau_operation().add(new JTable(tab.getDonnees_tableau().get(tab.getDonnees_tableau().size()-1)));
						
						tab.getListe_scroll().add(new JScrollPane()); 
					
						tab.getListe_scroll().get(tabbedPane_centre.getComponentCount()).setViewportView(tab.getTableau_operation().get(tabbedPane_centre.getComponentCount()));
						tabbedPane_centre.add(tab.getListe_scroll().get(tabbedPane_centre.getComponentCount()), BorderLayout.CENTER);
						tabbedPane_centre.setTitleAt(tabbedPane_centre.getComponentCount()-1, frame_add_compte.getNomCompte());
							
						tab.getListe_scroll().get(tabbedPane_centre.getComponentCount()-1).setViewportView(tab.getTableau_operation().get(tabbedPane_centre.getComponentCount()-1));
						
						tab.getListe_comptes().add(frame_add_compte.getCompte());
						
						// fermer la fenetre d'ajout de compte
						frame_add_compte.dispose();
						System.out.println("liste compte");
						System.out.println(tab.getListe_comptes());
						// Les boutons ajouter op�rations, Sauvegarde et G�n�rer Rapport peuvent �tre activ�.
						btnAjouterUneOpration.setEnabled(true);
						btnDeleteOperation.setEnabled(true);
						btnGnrerRapport.setEnabled(true);
						btnSauvegarder.setEnabled(true);
						
						// La sauvegarde n'est plus � jour. Afficher cette information � l'utilisateur
						lbl_etat.setText("");
						
						// Mettre � jour le montant disponible.
						lblMontant.setText(RecalculerSolde()+ " �");
					}
				});
				
					
				}
		});
		
				btnAddCompte.setToolTipText("");
				panel_gauche.add(btnAddCompte, "1, 3, fill, fill");
		
		JButton button_1 = new JButton("Mentions l\u00E9gales");
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				mention_legale_ui = new MentionsLegalesUI();
				mention_legale_ui.setVisible(true);
				
			}
		});
		
		JButton button = new JButton("Information compte ");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				information_compte_ui = new InformationCompteUI(tab.getListe_comptes().get(tabbedPane_centre.getSelectedIndex()));
				information_compte_ui.setVisible(true);
				
			}
		});
		panel_gauche.add(button, "1, 4");
		panel_gauche.add(button_1, "1, 14");
		
		
		JLabel lbl_image = null;
		// Si le logo est dans le dossier logo
		File logo = new File("logo/Logo_BL2P.png");
		if(logo.exists())
		{
			lbl_image = new JLabel(new ImageIcon("logo/Logo_BL2P.png"));
			
		}
		// Sinon l'imaage se trouve � la racine
		else
		{
			lbl_image = new JLabel(new ImageIcon(getClass().getResource("/Logo_BL2P.png")));
		}
		
		
		
		panel_gauche.add(lbl_image, "1, 20, center, bottom");
		
		JPanel panel_haut = new JPanel();
		contentPane.add(panel_haut, BorderLayout.NORTH);
		panel_haut.setLayout(new GridLayout(1, 0, 0, 0));
		
		btnAjouterUneOpration = new JButton("Ajouter une op\u00E9ration");
		
		// Si le bouton Ajouter une op�ration est press�
		btnAjouterUneOpration.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				ajouter_operation_ui = new AjouterOperationUI();
				ajouter_operation_ui.setVisible(true);
				ajouter_operation_ui.getButton().addActionListener(new ActionListener()
				{
					public void actionPerformed(ActionEvent e) {
						
						tab.getDonnees_tableau().get(tabbedPane_centre.getSelectedIndex()).addOperationToList(ajouter_operation_ui.getOperation());
						tab.getTableau_operation().get(tabbedPane_centre.getSelectedIndex()).setModel(tab.getDonnees_tableau().get(tabbedPane_centre.getSelectedIndex()));
						tab.getListe_scroll().get(tabbedPane_centre.getSelectedIndex()).setViewportView(tab.getTableau_operation().get(tabbedPane_centre.getSelectedIndex()));
						tab.getListe_comptes().get(tabbedPane_centre.getSelectedIndex()).Add_Opperation(ajouter_operation_ui.getOperation());
						
						ajouter_operation_ui.dispose(); 
						
						lblMontant.setText(RecalculerSolde()+ " �");
						
						// La sauvegarde n'est plus � jour. Afficher cette information � l'utilisateur
						lbl_etat.setText("");
					}
				});
				}
		});
		panel_haut.add(btnAjouterUneOpration);
		
		btnSauvegarder = new JButton("Sauvegarder");
		btnSauvegarder.addActionListener(new ActionListener() {
			// Si le bouton Sauvegarder est appuy�
			public void actionPerformed(ActionEvent arg0) {
				boolean state = false;
				Sauvegarde save = new Sauvegarde("sauvegardes/"+username+"_save.bl2p"); 
				state = save.Save(tab.getListe_comptes());
				
				if(state == true)
				{
					lbl_etat.setText("Sauvegard�");
				}
				else
				{
					lbl_etat.setText("Erreur de sauvegarde");
				}
			}
		});
		

		btnDeleteOperation = new JButton("Supprimer op\u00E9ration(s)");
		panel_haut.add(btnDeleteOperation);
		btnDeleteOperation.addActionListener(new ActionListener() {
			// Si le bouton Supprimer operation est cliqu�
			public void actionPerformed(ActionEvent e) {
				
				SupprimerOperationUI confirm_delete = new SupprimerOperationUI();
				confirm_delete.setVisible(true);
				
				confirm_delete.getCancelButton().addActionListener(new ActionListener() {
					
					@Override
					public void actionPerformed(ActionEvent e) {
						confirm_delete.setVisible(false);
						confirm_delete.dispose();
					}
				});
				
				confirm_delete.getOkButton().addActionListener(new ActionListener() {
					
					@Override
					public void actionPerformed(ActionEvent e) {
						confirm_delete.setVisible(false);
						confirm_delete.dispose();
					
						// Supprimer les op�rations s�lectionn�es
						for(int i = 0; i< tab.getDonnees_tableau().get(tabbedPane_centre.getSelectedIndex()).getRowCount(); i++)
						{
							if((boolean) tab.getDonnees_tableau().get(tabbedPane_centre.getSelectedIndex()).getValueAt(i, 0))
							{
								tab.getDonnees_tableau().get(tabbedPane_centre.getSelectedIndex()).removeOperationFromList(i);
								tab.getListe_comptes().get(tabbedPane_centre.getSelectedIndex()).getListeOperations().remove(i);
								
							}
						}
						// Actualisation de l'affichage (du tableau)
						tab.getTableau_operation().get(tabbedPane_centre.getSelectedIndex()).setModel(tab.getDonnees_tableau().get(tabbedPane_centre.getSelectedIndex()));
						tab.getListe_scroll().get(tabbedPane_centre.getSelectedIndex()).setViewportView(tab.getTableau_operation().get(tabbedPane_centre.getSelectedIndex()));
						
						// Recalcul du montant disponible pour le compte
						lblMontant.setText(tab.getListe_comptes().get(tabbedPane_centre.getSelectedIndex()).Refresh_solde() + " �");
						
					}
				});
			
			
			}
		});
		panel_haut.add(btnSauvegarder);
		
		btnGnrerRapport = new JButton("G\u00E9n\u00E9rer Rapport");
		
		btnGnrerRapport.addActionListener(new ActionListener() {
			// Si le bouton d'�dition d'un rapport est cliqu�
			public void actionPerformed(ActionEvent arg0) {
				
				rapportUI = new GenererRapportUI(tab.getListe_comptes());
				rapportUI.setVisible(true);
				rapportUI.getBtnGnrer().addActionListener(new ActionListener() {
					
					@Override
					public void actionPerformed(ActionEvent e) {
						
						// V�rifier que le rapport � bien �t� g�n�r�s et afficher l'�tat correspondant
						if(!rapportUI.isEtat_generation_rapportOK())
						{
							lbl_etat.setText("Etat : Le rapport a �t� �dit�");
						}
						else
						{
							lbl_etat.setText("Etat : Erreur durant la g�n�ration du rapport");
						}
						
						rapportUI.dispose();
						
					}
				});
				
			}
		});
		panel_haut.add(btnGnrerRapport);
		
		JLabel lblSolde = new JLabel("Solde");
		panel_haut.add(lblSolde);
		
		
		panel_haut.add(lblMontant);
		
		ancienne_valeur = -1;
		tabbedPane_centre.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) {
				if((ancienne_valeur == -1) && (tabbedPane_centre.getSelectedIndex() == 0))
					ancienne_valeur = tabbedPane_centre.getSelectedIndex();
				else
					lblMontant.setText(RecalculerSolde()+ " �");
			}
		});
		contentPane.add(tabbedPane_centre, BorderLayout.CENTER);
		
		lbl_etat = new JLabel("");
		contentPane.add(lbl_etat, BorderLayout.SOUTH);
		
		// Griser les cases si aucun compte n'est cr��
		if(tabbedPane_centre.getSelectedIndex() == -1)
		{
			btnAjouterUneOpration.setEnabled(false);
			btnDeleteOperation.setEnabled(false);
			btnGnrerRapport.setEnabled(false);
			btnSauvegarder.setEnabled(false);
			
		}
		else
		{
			btnAjouterUneOpration.setEnabled(true);
			btnDeleteOperation.setEnabled(true);
			btnGnrerRapport.setEnabled(true);
			btnSauvegarder.setEnabled(true);
		}

	}
	
	/**
	 * M�thode permettant de recalculer le montant disponible sur le compte
	 */
	public double RecalculerSolde(){
		double montant = 0;
		if(tabbedPane_centre.getSelectedIndex() != -1)
		{
			 montant = tab.getListe_comptes().get(tabbedPane_centre.getSelectedIndex()).Refresh_solde();
		}
		return montant;
	}
}