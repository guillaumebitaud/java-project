import java.util.Date;

/**
 * Class Compte_Courant qui h�rite de Compte
 * @author Pierre LE POTTIER
 *
 */
class Compte_Courant extends Compte {
	
	
	// A utiliser dans le cas d'une s�rialisation de cette class
	private static final long serialVersionUID = -4681720025370800322L;

	/**
	 * Constructeur par d�faut
	 */
	public Compte_Courant(String description_def, double montant_dispo_def, String name)
	{
		super(description_def, montant_dispo_def, name); //Appel du constructeur de la classe m�re
			
	}
	
	public Compte_Courant(String name, String description_def, double montant_dispo_def, Date date_compte_def,double solde_mini)
	{
		super(name, description_def, montant_dispo_def,date_compte_def, solde_mini); //Appel du constructeur de la classe m�re
			
	}
	
/**
 * Test unitaire	
 * @param args
 */
public static void main(String[] args) {
		
		Compte_Courant courant = new Compte_Courant("Mister you" , 500, "Compte Courant");
		System.out.println(courant);
		
		Date date = new Date();
		Operation opp1 = new Operation(date, 200,false, "Epargne", "Primes");
		Operation opp2 = new Operation(date, 200,false, "Maison vendue", "Exceptionnel");
		
		courant.Add_Opperation(opp1);
		System.out.println(" Avoir disponible :  " + courant.getMontant_dispo() + "�");
		
		courant.Add_Opperation(opp2);
		System.out.println(" Avoir disponible :  " + courant.getMontant_dispo() + "�");

	}

	/**
	 * Impl�mentation de la m�thode toString()
	 */
	public String toString()
	{
		return super.toString() + "de type Compte Courant";
	}
}
