import java.awt.Button;
import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Class permettant de sauvegarder et de r�cup�rer les donn�es sauvegard�es
 * @author Guillaume BITAUD
 */
public class Sauvegarde {

	// Nom du fichier de sauvegarde
	private String nom_fichier;
	
	// Fichier 
	private File fichier;
	private FileWriter fwriter;
    
	/**
	 * @brief Constructor
	 */
	public Sauvegarde(String n_fichier){
		nom_fichier = n_fichier;
		fichier = new File(nom_fichier);
		fwriter = null;
	}
	
	/**
	 * @brief Fonction permettant de sauvegarder le param�tre txt_a_sauvegarder dans le fichier de sauvegarde. 
	 * @param txt_a_sauvegarder Texte � sauvegarder dans le fichier de sauvegarde.
	 * @return Retourne true si la sauvegarde est un succ�s. Sinon, retourne false.
	 */
	public boolean Save(String txt_a_sauvegarder){
		
		boolean retour = false;
		
		// Essayer d'ouvrir le fichier en �criture
		try {
			fwriter = new FileWriter(fichier);
			
			// Ecrire dans le fichier de sauvegarde
			fwriter.write(txt_a_sauvegarder);
			
			// Fermer le flux
			fwriter.close();
			
			// Modification de la valeur de retour de la fonction
			retour = true;
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return retour;		
	}
	
	/**
	 * @brief Sauvegarde un objet dans le fichier de sauvegarde (S�rialisation binaire). 
	 * @param object_to_save Objet � s�rialiser.
	 * @return retourne true si la s�railisation a fonctionn�e, sinon retourne false.s
	 */
	public boolean Save(Object object_to_save){
		boolean retour = false;
		
		try {
			// Ouverture d'un flux permettant de sauvegarder les objets
			ObjectOutputStream flux_objet =  new ObjectOutputStream(new FileOutputStream(fichier));
			
			// Ecriture de l'objet dans le fichier (s�rialisation)
			flux_objet.writeObject(object_to_save);
			
			flux_objet.flush();
			
			// Fermeture du flux
			flux_objet.close();
			
			// Modification de la valeur de retour de la fonction
			retour = true;
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return retour;
	}
	
	/**
	 * @brief Sauvegarde un objet dans le fichier de sauvegarde (S�rialisation XML)
	 * @param object_to_save Objet � s�rialiser.
	 * @return Retourne true si l'op�ration de s�rialisation s'est correctement d�roul�e. 
	 */
	public boolean SaveXML(Object object_to_save){
		boolean retour = false;
		
		try {
			
			// Ouverture d'un flux vers le fichier de sauvegarde en mode XML
			XMLEncoder encoder = new XMLEncoder(new BufferedOutputStream( new FileOutputStream(fichier)));
			
			// Serialisation de l'objet.
			encoder.writeObject(object_to_save);
			
			// Ecriture de l'objet � s�rialiser dans le fichier de sauvegarde 
			encoder.flush();
			
			// Fermeture du flux vers le fichier
			encoder.close();
			
			// Modification de la valeur de retour de la fonction
			retour = true;			
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return retour; 
	}
	
	
	/**
	 * @brief Fonction permettant de sauvegarder plusieurs objets dans le fichier de sauvegarde (S�rialisation XML).
	 * @param list_to_save Liste contenant les objets � s�rialiser.
	 * @return Retourne true si l'op�ration de s�rialisation est un succ�s. 
	 */
	public boolean SaveXML(ArrayList<Object> list_to_save){
		boolean retour = false; 
		
		try {
			
			// Ouverture d'un flux vers le fichier de sauvegarde en mode XML
			XMLEncoder encoder = new XMLEncoder(new BufferedOutputStream( new FileOutputStream(fichier)));
			
			// Serialisation de l'objet.
			encoder.writeObject(list_to_save);
			
			// Ecriture de l'objet � s�rialiser dans le fichier de sauvegarde 
			encoder.flush();
			
			// Fermeture du flux vers le fichier
			encoder.close();
			
			// Modification de la valeur de retour de la fonction
			retour = true;			
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return retour;
	}
	/**
	 * @brief Fonction permettant de lire int�gralement le fichier de sauvegarde
	 * @return
	 */
	public Object Load(){
		Object retour = null;
		
		FileInputStream fis;
				
			
			try {
				// ouverture d'un flux d'entr�e depuis le fichier fichier
				fis = new FileInputStream(fichier);
				// cr�ation d'un "flux objet" avec le flux fichier
				ObjectInputStream ois= new ObjectInputStream(fis);
				try {	
					// d�s�rialisation : lecture de l'objet depuis le flux d'entr�e
					retour = ois.readObject(); 
				} finally {
					// on ferme les flux
					try {
						ois.close();
					} finally {
						fis.close();
					}
				}
			} catch(IOException ioe) {
				ioe.printStackTrace();
			} catch(ClassNotFoundException cnfe) {
				cnfe.printStackTrace();
			}
				
		return retour;
	}
	
	/**
	 * @brief Fonction permettant de charger un objet s�rialis� en XML
	 * @return Retourne l'objet d�s�rialis�.
	 */
	public Object LoadXML(){
		
		Object retour = null;
		
		XMLDecoder decoder = null;
		
		
		try {
			decoder = new XMLDecoder(new BufferedInputStream(new FileInputStream(fichier)));
			retour = decoder.readObject();
			
			decoder.close();
			} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return retour;
	}
	
	/**
	 * @brief Fonction permettant de charger un fichier XML contenant plusieurs objets
	 * @return Retourne une liste d'objets charg�s depuis le fichier de sauvegarde.
	 */
	public ArrayList<Object> LoadArrayXML(){
		
		ArrayList<Object> liste = new ArrayList<Object>();
		
		XMLDecoder decoder = null;
				
		try {
			decoder = new XMLDecoder(new BufferedInputStream(new FileInputStream(fichier)));
			
			liste.add(decoder.readObject());
			
			decoder.close();
			} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return liste;
		
	}
	
	/**
	 * @bief Test unitaire
	 * @param args
	 */
	public static void main(String[] args) {
		// Cr�er un objet Sauvegarde
		Sauvegarde Test_Sauvegarde = new Sauvegarde("sauvegardes/test_sauvegarde.bl2p");
		
		// Sauvegarder le mot "Hello World" dans le fichier de sauvegarde
		/*
		 * Test de la m�thode Save en mode texte
		 * Etat du test : OK 
		 */
		//Test_Sauvegarde.Save("Hello World");
		
		// Cr�ation d'un objet de type bouton afin de tester la s�rialisation binaire et XML. 
		Button test_serialization;
		test_serialization = new Button();
		test_serialization.setName("Nom");
		test_serialization.setLabel("Labeltest");
		
		Date test_date = new Date(); 
		test_date.getTime();
		
		/****************************************************/
		/* 		Tests Unitaires								*/
		/****************************************************/
		
		/* 
		 * Test de la m�thode Save (s�rialisation binaire)
		 * Etat du test : OK
		 */
		//Test_Sauvegarde.Save(test_serialization);
		
		/*
		 *  Test de la m�thode SaveXML (s�rialisation XML)
		 *  Etat du test : OK
		 */
		//Test_Sauvegarde.SaveXML(test_serialization);
		
		/*
		 * Test de la m�thode LoadXML (d�s�rialisation XML)
		 * Etat du test : OK 
		 */
		//final Button test_restauration = (Button) Test_Sauvegarde.LoadXML();
		//System.out.println("Test de la fonction LoadXML : " + test_restauration);
		
		/*
		 * Test de la s�rialisation de plusieurs objets dans le fichier de sauvegarde (s�rialisation XML)
		 * Etat du test : OK
		 */
		List<Object> liste = new ArrayList<Object>();
		liste.add(test_date);
		liste.add(test_serialization);
		
		Test_Sauvegarde.SaveXML(liste);
		
		
		/*
		 * Test du chargement de plusieurs objets stock�es dans le fichier de sauvegarde (s�rialisation XML)
		 * Etat du test : OK
		 */
		List<Object> liste_retour = new ArrayList<Object>();
		liste_retour = Test_Sauvegarde.LoadArrayXML();
		
		System.out.println(liste_retour);
		
	}

}
