import java.awt.EventQueue;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;

import java.awt.Font;

import javax.swing.SwingConstants;

import java.awt.FlowLayout;
import java.awt.SystemColor;
import java.io.File;

/**
 * Class permettant d'afficher la fen�tre Mentions L�gales
 * @author Pierre LE POTTIER
 *
 */
public class MentionsLegalesUI extends JFrame {

	// A utiliser dans le cas d'une s�rialisation de cette class
	private static final long serialVersionUID = -7008803419302549763L;
	
	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MentionsLegalesUI frame = new MentionsLegalesUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MentionsLegalesUI() {
		setForeground(SystemColor.controlDkShadow);
		setTitle("Mentions l\u00E9gales");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 423, 305);
		contentPane = new JPanel();
		contentPane.setForeground(SystemColor.menu);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		JLabel lbl_image_BL2P = null;
		// Si le logo est dans le dossier logo
		File logo = new File("logo/Logo_BL2P.png");
		if(logo.exists())
		{
			lbl_image_BL2P = new JLabel(new ImageIcon("logo/Logo_BL2P.png"));
			
		}
		// Sinon l'imaage se trouve � la racine
		else
		{
			lbl_image_BL2P = new JLabel(new ImageIcon(getClass().getResource("/Logo_BL2P.png")));
		}
		
		
		lbl_image_BL2P.setHorizontalAlignment(SwingConstants.LEFT);
		contentPane.add(lbl_image_BL2P);
		
		JLabel lbl_copyright = new JLabel("2015 \u00A9 Copyright -BL2P S.A.R.L- Tous droits r\u00E9serv\u00E9s");
		lbl_copyright.setFont(new Font("Tahoma", Font.PLAIN, 14));
		contentPane.add(lbl_copyright);
		
		JLabel lbl_toute_copie = new JLabel("Toute copie, reproduction ou diffusion totale ou partielle\r\n du contenu");
		lbl_toute_copie.setVerticalAlignment(SwingConstants.TOP);
		lbl_toute_copie.setFont(new Font("Tahoma", Font.PLAIN, 9));
		contentPane.add(lbl_toute_copie);
		
		JLabel lbl_des_images = new JLabel("des images et des textes pr\u00E9sents dans ce \r\nlogiciel est totalement ");
		lbl_des_images.setFont(new Font("Tahoma", Font.PLAIN, 9));
		contentPane.add(lbl_des_images);
		
		JLabel lbl_interdit = new JLabel("interdit sans l'accord pr\u00E9alablement \u00E9crit de la soci\u00E9t\u00E9 BL2P S.A.R.L");
		lbl_interdit.setFont(new Font("Tahoma", Font.PLAIN, 9));
		contentPane.add(lbl_interdit);
	}

}
