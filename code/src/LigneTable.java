import java.util.Date;


/**
 * Class permettant de d�finir une ligne d'op�ration dans le tableau.
 * Une ligne est compos�e d'une checkbox � gauche puis des informations relatives � l'op�ration
 * @author Guillaume BITAUD
 *
 */
public class LigneTable {

	private Operation operation;
	private Boolean check;
	
	/**
	 * Constructeur par d�faut
	 * @param op
	 */
	public LigneTable(){
		
		check 		= false;
	}
	
	/**
	 * Constructeur prenant en parametre une operation
	 * @param op
	 */
	public LigneTable(Operation op){
		
		operation 	= op;
		check 		= false;
	}
	
	/**
	 * Obtenir l'operation de la ligne
	 * @return
	 */
	public Operation getOperation() {
		return operation;
	}
	
	/**
	 * Retourne la date de l'op�ration
	 * @return
	 */
	public Date getOperationDate()
	{
		return operation.getDate_transaction();
	}
	
	/**
	 * Retourne le montant de l'op�ration
	 * @return
	 */
	public double getOperationMontant(){
		return operation.getMontant();
	}
	
	/**
	 * Retourne "D�bit" ou "Cr�dit" en fonction du type d'op�ration
	 * @return
	 */
	public String getOperationDebitCredit()
	{
		if(operation.isDebit_credit())
		{
			return "Cr�dit";
		}
		else
		{
			return "D�bit";
		}
	}
	
	
	/**
	 * Retourne la description de l'op�ration
	 * @return
	 */
	public String getOperationDescription(){
		return operation.getDescription();
	}
	
	/**
	 * Retourne l'�tiquette de l'op�ration
	 * @return
	 */
	public String getOperationEtiquette(){
		return operation.getEtiquette();
	}
	
	/**
	 * Mettre � jour l'op�ration de la ligne
	 * @param operation
	 */
	public void setOperation(Operation operation) {
		this.operation = operation;
	}

	/**
	 * Obtenir la valeur de la checkbox
	 * @return
	 */
	public Boolean getCheck() {
		return check;
	}

	/**
	 * Mettre � jour la valeur de la checkbox
	 * @param check
	 */
	public void setCheck(Boolean check) {
		this.check = check;
	}
	
	/**
	 * Tests Unitaires
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
