import java.util.ArrayList;
import java.util.Date;

import javax.swing.JCheckBox;
import javax.swing.table.AbstractTableModel;

/**
 * Class repr�sentant un mod�le du tableau. 
 * Ce mod�le est utilis� dans le tableau centrale de l'application
 * @author Guillaume
 *
 */
public class ModeleData extends AbstractTableModel { 
	
	// A d�finir pour permettre la s�rialisation de cette Class.
	private static final long serialVersionUID = 9167642508498694859L;
	
	// Tableau de String contenant les ent�tes du tableau.
	private final String[] entetes = {"  ", "Date", "Etiquette", "Montant", "D�bit/Cr�dit"};
	
	// Liste contenant les op�rations
	private ArrayList<LigneTable> data_lignes;
	
	/**
	 * Constructeur par d�faut
	 */
	public ModeleData() {
		// Appel du constructeur de la class m�re.
		super();
		data_lignes = new ArrayList<LigneTable>();
		
	}
	
	/**
	 * Ajouter une op�ration � la liste des op�ration
	 * @param op
	 */
	public void addOperationToList(Operation op, int index){
		data_lignes.get(index).setOperation(op);
	}
	
	/**
	 * Ajoute l'op�ration � la fin de la liste 
	 * @param op
	 */
	public void addOperationToList(Operation op){
		data_lignes.add(new LigneTable(op));
	}
	
	public void removeOperationFromList(int index){
		data_lignes.remove(index);
	}
	
	public String getColumnName(int col){
		return entetes[col];
	}
	
	@Override
	public int getColumnCount() {
		
		return entetes.length;
	}
	@Override
	public int getRowCount() {
		
		return (data_lignes.size());
	}
	@Override
	public Object getValueAt(int index_ligne, int index_colonne) {
		
		switch(index_colonne){
		case 0:
			return data_lignes.get(index_ligne).getCheck();
		case 1:
			return data_lignes.get(index_ligne).getOperationDate();
		case 2:
			return data_lignes.get(index_ligne).getOperationEtiquette();
		case 3:
			return data_lignes.get(index_ligne).getOperationMontant();
		case 4:
			return data_lignes.get(index_ligne).getOperationDebitCredit();
		default:
			return null;
		}
	}
	
	@Override
	public Class<?> getColumnClass(int colNum)
	{
		switch(colNum){
			case 0 :
				return Boolean.class;
			case 1 :
				return Date.class;
			case 2 :
				return String.class;
			case 3 :
				return Double.class;
			case 4 :
				return String.class;
			default:
				return String.class;	
		}
	}
	
	
	@Override
    public void setValueAt(Object inValue, int inRow, int inCol) {
        if(inRow < 0 || inCol < 0 || inRow >= getRowCount() )
            return;

        switch (inCol) {
            case 0 : data_lignes.get(inRow).setCheck((Boolean)inValue);
            	break;
            default: 
            	break;
            }
        fireTableCellUpdated(inRow, inCol);
        }	
	
	@Override
	public boolean isCellEditable(int row, int col){
        if(col == 0){
            return true;
        }
        return false;
    }
	
	
	/**
	 * Implementation de la m�thode toString
	 */
	public String toString(){
		String retour = null;
		
		for(int i =0; i<data_lignes.size();i++)
		{
			retour += "Ligne n�"+i+"\nDat� du : "+data_lignes.get(i).getOperationDate()
				   +"\nEtiquette :"+data_lignes.get(i).getOperationEtiquette()
				   +"\nMontant : "+data_lignes.get(i).getOperationMontant()+"\n";
		}
		return retour;
	}
	
	/**
	 * Tests Unitaires
	 * @param args
	 */
	public static void main(String[] args) {
		
		ModeleData test_mod;
		test_mod = new ModeleData();
		
		/**
		 * Test de la cr�ation d'un objet de type ModeleData, ajout d'op�rations et affichage
		 * Etat du Test : OK
		 */
		test_mod.addOperationToList(new Operation(new Date(), 100, false, "Ceci est un test", "Test"));
		System.out.println(test_mod);
		test_mod.addOperationToList(new Operation(new Date(), 200, false, "Ceci est un test1", "Test2"));
		System.out.println(test_mod);
		
	}
}
