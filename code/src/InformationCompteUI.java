import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.RowSpec;
import com.jgoodies.forms.factories.FormFactory;
import javax.swing.SwingConstants;

/**
 * Class affichant la fen�tre d'information de compte
 * @author Pierre LE POTTIER
 *
 */
public class InformationCompteUI extends JFrame {

	// A utiliser dans le cas d'une s�rialisation de cette class
	private static final long serialVersionUID = -1149227785604921936L;
	
	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					InformationCompteUI frame = new InformationCompteUI(new Compte("",0.0, "Test"));
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * 
	 */
	/**
	 * Create the frame.
	 * @param cpt Compte � partir duquel afficher les informations
	 */
	public InformationCompteUI(Compte cpt) {
		setTitle("Informations du compte");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new FormLayout(new ColumnSpec[] {
				ColumnSpec.decode("52px"),
				ColumnSpec.decode("151px"),
				FormFactory.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("max(87dlu;default):grow"),
				FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC,
				FormFactory.RELATED_GAP_COLSPEC,
				FormFactory.DEFAULT_COLSPEC,},
			new RowSpec[] {
				RowSpec.decode("62px"),
				RowSpec.decode("23px"),
				FormFactory.RELATED_GAP_ROWSPEC,
				RowSpec.decode("max(15dlu;default)"),
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,
				FormFactory.RELATED_GAP_ROWSPEC,
				FormFactory.DEFAULT_ROWSPEC,}));
		
		JLabel lblTitreNomCompte = new JLabel("Nom du compte : ");
		contentPane.add(lblTitreNomCompte, "2, 2, right, center");
		
		JLabel lblNomCompte = new JLabel(cpt.getName());
		lblNomCompte.setHorizontalAlignment(SwingConstants.CENTER);
		contentPane.add(lblNomCompte, "4, 2, center, center");
		
		JLabel lblTitreDateCompte = new JLabel("Date ouverture compte :");
		lblTitreDateCompte.setHorizontalAlignment(SwingConstants.CENTER);
		contentPane.add(lblTitreDateCompte, "2, 4, right, default");
		
		JLabel lblDateCompte = new JLabel(cpt.date_compte.toString());
		contentPane.add(lblDateCompte, "4, 4, center, default");
		
		JLabel lblTitreInteret = new JLabel("Taux d'int\u00E9ret :");
		lblTitreInteret.setHorizontalAlignment(SwingConstants.CENTER);
		contentPane.add(lblTitreInteret, "2, 6, right, default");
		
		JLabel lblInteret = new JLabel();
		if(cpt instanceof Compte_Epargne)
		{
			lblInteret.setText(" "+((Compte_Epargne) cpt).getInteret());
		}
		
		contentPane.add(lblInteret, "4, 6, center, default");
		
		JLabel lbl_pourcent = new JLabel("%");
		contentPane.add(lbl_pourcent, "6, 6, left, default");
		
		JLabel lblTitreSolde = new JLabel("Solde Actuel :");
		lblTitreSolde.setHorizontalAlignment(SwingConstants.CENTER);
		contentPane.add(lblTitreSolde, "2, 8, right, default");
		
		JLabel lblSolde = new JLabel(String.valueOf(cpt.getMontant_dispo()));
		contentPane.add(lblSolde, "4, 8, center, default");
		
		JLabel lbl_euro = new JLabel("\u20AC");
		contentPane.add(lbl_euro, "6, 8, right, bottom");
		
		JLabel lblSoldeInitial = new JLabel("Solde initial :");
		contentPane.add(lblSoldeInitial, "2, 10, right, default");
		
		JLabel lbl_solde_initial = new JLabel(String.valueOf(cpt.getSolde_init()));
		contentPane.add(lbl_solde_initial, "4, 10, center, default");
		
		JLabel lbl_euro2 = new JLabel("\u20AC");
		contentPane.add(lbl_euro2, "6, 10, right, bottom");
		
		JLabel lblTitreDecouvert = new JLabel("D\u00E9couvert autoris\u00E9 :");
		lblTitreDecouvert.setHorizontalAlignment(SwingConstants.CENTER);
		contentPane.add(lblTitreDecouvert, "2, 12, right, default");
		
		JLabel lblDecouvert = new JLabel(String.valueOf(cpt.getSolde_minimum()));
		contentPane.add(lblDecouvert, "4, 12, center, default");
		
		JLabel lbl_euro3 = new JLabel(" \u20AC");
		lbl_euro3.setHorizontalAlignment(SwingConstants.RIGHT);
		contentPane.add(lbl_euro3, "6, 12");
		
		JLabel lblDescription = new JLabel("Description : ");
		contentPane.add(lblDescription, "2, 14, right, default");
		
		JLabel lblLbldescriptioncompte = new JLabel(cpt.getDescription());
		contentPane.add(lblLbldescriptioncompte, "4, 14, center, default");
	}

}
