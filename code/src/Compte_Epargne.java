import java.util.Date;




/**
 * Class Compte_Epargne qui h�rite de Compte
 * @author Pierre
 *
 */
class Compte_Epargne extends Compte {

	// A utiliser dans le cas d'une s�rialisation de cette class
	private static final long serialVersionUID = -9078557274003199191L;
	
	private double prevision;
	private double interet;
	
	/**
	 * Constructeur par d�faut
	 */
	public Compte_Epargne(String description_def, double montant_dispo_def, String name)
	{
		super(description_def,montant_dispo_def, name);
		interet = 0;
		prevision = 0;
		
		
	}
	
	public Compte_Epargne(String description_def, double montant_dispo_def, double inter, double prev, String name)
	{
		super(description_def,montant_dispo_def, name);
		interet = inter;
		prevision = prev;
				
	}
	
	public Compte_Epargne(String name, String description, double montant_dispo_def, double inter, double prev, Date date_compte_epargne, double solde_mini)
	{
		
		super(name, description,montant_dispo_def,date_compte_epargne,solde_mini);
		interet = inter;
		prevision = prev;
		
	}
	
	/**
	 * Test unitaire de la class Compte_Epargne
	 * @param args
	 */
	public static void main(String[] args) {
		
		Compte_Epargne epargne = new Compte_Epargne("Pierre LE POTTIER", 5000, 1.7 , 500, "Compte Epagne");
		System.out.println(epargne);
		
		Date date = new Date();
		Operation opp1 = new Operation(date, 200,false, "Epargne", "Primes");
		Operation opp2 = new Operation(date, 200,false, "Maison vendue", "Exceptionnel");
		
		epargne.Add_Opperation(opp1);
		System.out.println(" Avoir disponible :  " + epargne.getMontant_dispo() + "�");
		
		epargne.Add_Opperation(opp2);
		System.out.println(" Avoir disponible :  " + epargne.getMontant_dispo() + "�");

	}
	
	
	public String toString()
	{
		return super.toString() + "de type compte epargne et d'int�r�t : " + interet;		
	}
	
	
	public double getPrevision() {
		return prevision;
	}

	public void setPrevision(double prevision) {
		this.prevision = prevision;
	}

	public double getInteret() {
		return interet;
	}

	public void setInteret(double interet) {
		this.interet = interet;
	}
	
	
}
