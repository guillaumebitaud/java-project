import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import java.awt.GridBagLayout;

import javax.swing.JLabel;

import java.awt.GridBagConstraints;
import java.awt.Font;
import java.awt.Insets;

import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.JPasswordField;
import javax.swing.JButton;

import com.sun.crypto.provider.BlowfishCipher;

import sun.security.provider.MD5;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.ArrayList;

/**
 * Class permettant d'afficher la fen�tre de d�marrage de l'application
 * La fen�tre cr��e demande un identifiant et un mot de passe ou permet d'en cr�er un. 
 * 
 * @author Guillaume BITAUD
 */
public class BL2P extends JFrame {

	// A utiliser dans le cas d'une s�rialisation de cette class
	private static final long serialVersionUID = -7612377280643954018L;
	
	private JPanel contentPane;
	private JTextField tf_id_connexion;
	private JPasswordField tf_password;
	
	// Gestion du fichier de sauvegarde des identifiants/mots de passe
	private File fichier_connexion;
	private FileWriter fwriter;
	
	private MainUI fenetre_principale;
	
	private JPasswordField tf_pwd_new_user;
	private JPasswordField tf_pwd_conf_new_user;
	private JTextField tf_id_new_user;
		private JLabel lbl_erreurs;
	
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					BL2P frame = new BL2P();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
				
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public BL2P() {
		setTitle("Connexion \u00E0 BL2P");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 526, 345);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		GridBagLayout gbl_contentPane = new GridBagLayout();
		gbl_contentPane.columnWidths = new int[]{0, 0, 0, 0, 0, 0, 0, 0};
		gbl_contentPane.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
		gbl_contentPane.columnWeights = new double[]{0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, Double.MIN_VALUE};
		gbl_contentPane.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		contentPane.setLayout(gbl_contentPane);
		
		JLabel lbl_Connexion = new JLabel("Connexion :");
		lbl_Connexion.setFont(new Font("Tahoma", Font.PLAIN, 16));
		GridBagConstraints gbc_lbl_Connexion = new GridBagConstraints();
		gbc_lbl_Connexion.insets = new Insets(0, 0, 5, 5);
		gbc_lbl_Connexion.gridwidth = 2;
		gbc_lbl_Connexion.gridx = 2;
		gbc_lbl_Connexion.gridy = 2;
		contentPane.add(lbl_Connexion, gbc_lbl_Connexion);
		
		JLabel lbl_identifiant_connexion = new JLabel("Identifiant");
		GridBagConstraints gbc_lbl_identifiant_connexion = new GridBagConstraints();
		gbc_lbl_identifiant_connexion.anchor = GridBagConstraints.EAST;
		gbc_lbl_identifiant_connexion.insets = new Insets(0, 0, 5, 5);
		gbc_lbl_identifiant_connexion.gridx = 3;
		gbc_lbl_identifiant_connexion.gridy = 3;
		contentPane.add(lbl_identifiant_connexion, gbc_lbl_identifiant_connexion);
		
		tf_id_connexion = new JTextField();
		GridBagConstraints gbc_tf_id_connexion = new GridBagConstraints();
		gbc_tf_id_connexion.insets = new Insets(0, 0, 5, 5);
		gbc_tf_id_connexion.fill = GridBagConstraints.HORIZONTAL;
		gbc_tf_id_connexion.gridx = 4;
		gbc_tf_id_connexion.gridy = 3;
		contentPane.add(tf_id_connexion, gbc_tf_id_connexion);
		tf_id_connexion.setColumns(10);
		
		JLabel lbl_MotDePasse_connexion = new JLabel("Mot de passe");
		GridBagConstraints gbc_lbl_MotDePasse_connexion = new GridBagConstraints();
		gbc_lbl_MotDePasse_connexion.anchor = GridBagConstraints.EAST;
		gbc_lbl_MotDePasse_connexion.insets = new Insets(0, 0, 5, 5);
		gbc_lbl_MotDePasse_connexion.gridx = 3;
		gbc_lbl_MotDePasse_connexion.gridy = 4;
		contentPane.add(lbl_MotDePasse_connexion, gbc_lbl_MotDePasse_connexion);
		
		tf_password = new JPasswordField();
		GridBagConstraints gbc_tf_password = new GridBagConstraints();
		gbc_tf_password.insets = new Insets(0, 0, 5, 5);
		gbc_tf_password.fill = GridBagConstraints.HORIZONTAL;
		gbc_tf_password.gridx = 4;
		gbc_tf_password.gridy = 4;
		contentPane.add(tf_password, gbc_tf_password);
		
		JLabel lbl_erreurs_connexion = new JLabel("");
		GridBagConstraints gbc_lbl_erreurs_connexion = new GridBagConstraints();
		gbc_lbl_erreurs_connexion.gridwidth = 2;
		gbc_lbl_erreurs_connexion.insets = new Insets(0, 0, 5, 5);
		gbc_lbl_erreurs_connexion.gridx = 3;
		gbc_lbl_erreurs_connexion.gridy = 6;
		contentPane.add(lbl_erreurs_connexion, gbc_lbl_erreurs_connexion);
		
		
		JLabel lblNouvelUtilisateur = new JLabel("Nouvel Utilisateur :");
		lblNouvelUtilisateur.setFont(new Font("Tahoma", Font.PLAIN, 16));
		GridBagConstraints gbc_lblNouvelUtilisateur = new GridBagConstraints();
		gbc_lblNouvelUtilisateur.insets = new Insets(0, 0, 5, 5);
		gbc_lblNouvelUtilisateur.gridwidth = 2;
		gbc_lblNouvelUtilisateur.gridx = 2;
		gbc_lblNouvelUtilisateur.gridy = 7;
		contentPane.add(lblNouvelUtilisateur, gbc_lblNouvelUtilisateur);
		
		/***********************************************************
		 *  Tester l'existance d'un dossier et d'un fichier de sauvegarde 
		 */
		fichier_connexion = new File("sauvegardes/connexion.bl2p");
		File dossier_sauvegardes = new File("sauvegardes");
		// Tester si le dossier sauvegarde existe
		if(!(dossier_sauvegardes.exists()) || !(dossier_sauvegardes.isDirectory()))
		{
			// Cr�er le dossier sauvegardes si besoin
			dossier_sauvegardes.mkdir();
		}
		
		// Tester si le fichier existe
		if(!fichier_connexion.exists())
		{
			// Si le fichier n'existe pas, le cr�er. 
			try {
				fwriter = new FileWriter(fichier_connexion);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		/*****************************************************/
		
		
		
		JButton btnConnexion = new JButton("Connexion");
		btnConnexion.addActionListener(new ActionListener() {
			// Si l'utilisateur clique sur le bouton de connexion
			public void actionPerformed(ActionEvent arg0) {
				
				// V�rifier que le mot de passe et l'identifiant sont correcte
				BufferedReader lecture_fichier;
				try {
					lecture_fichier = new BufferedReader(new FileReader(fichier_connexion));
					try {
						boolean fin = false;
						boolean ok  = true;
						String line;
						
						// Chercher l'identifiant
						while(((line = lecture_fichier.readLine()) != null) && (fin == false))
						{
							
							// Si l'identifiant a �t� trouv�
							if(tf_id_connexion.getText().equals(line))
							{
								System.out.println("line :" + line);
								System.out.println("TextField connexion :" + tf_id_connexion.getText());
								System.out.println("comparaison : "+ tf_id_connexion.getText().equals(line));
								
								System.out.println(tf_password.getPassword().toString());
								// Hacher le mot de passe entr� par l'utilisateur pour le compar� au mot de passe stock�
								String mdp_hash = get_SHA_1_SecurePassword(tf_password.getText());
								
								char pwd[] = mdp_hash.toCharArray();
								
								char pwd_true[] = lecture_fichier.readLine().toCharArray();	// lire le mot de passe � la ligne suivante du fichier
								
								System.out.println("taille pwd true" + pwd_true.length + " taille pwd : "+pwd.length);
								
								// Si les tailles des mots de passes sont �gales
								if(pwd.length == pwd_true.length)
								{
									for(int i = 0; i< tf_password.getPassword().length; i++)
									{
										System.out.println("ref : "+pwd_true[i] + " | a tester : "+ pwd[i]);
										System.out.println(pwd[i] != pwd_true[i]);
										if(pwd[i] != pwd_true[i])
										{
											ok = false;	// Indiquer que les deux mots de passes ne sont pas identiques
										}
									}
									// Si tous les caract�res des mots de passes sont identiques deux � deux
									if(ok == true)
									{
										fin = true;	// Terminer la boucle
										System.out.println("INSIDE");
									}
								}
								else
								{
									fin = true;	// Terminer la boucle
									ok = false;	// Indiquer que les deux mots de passes ne sont pas identiques
								}
							}
						}
						
						// Si le mot de passe a �t� trouv�
						if((fin == true) && (ok == true))
						{
							// Tester si une sauvegarde existe :
							File old_save = new File("sauvegardes/"+tf_id_connexion.getText()+"_save.bl2p");
							
							// Si la sauvegarde existe, la charger
							if(old_save.exists())
							{
								// Ouvrir la sauvegarde correspondante et charger la MainUI avec cette sauvegarde
								Sauvegarde save = new Sauvegarde("sauvegardes/"+tf_id_connexion.getText()+"_save.bl2p");
								
								// Cr�er la fen�tre principale avec les donn�es charg�es en param�tres
								fenetre_principale = new MainUI((ArrayList<Compte>)save.Load(),tf_id_connexion.getText());
								
							}
							// Sinon ouvrir une fen�tre vide
							else
							{
								// Cr�er la fen�tre principale vide
								fenetre_principale = new MainUI(null,tf_id_connexion.getText());
								
							}
							
							// Afficher la fen�tre principale
							fenetre_principale.setVisible(true);
							
							// Rendre cette fen�tre de connexion invisible 
							setVisible(false);
						}
						else
						{
							lbl_erreurs_connexion.setText("Erreur : Mauvais identifiant ou mot de passe");
						}
						
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				} catch (FileNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
					
				
			}
		});
		
		
		GridBagConstraints gbc_btnConnexion = new GridBagConstraints();
		gbc_btnConnexion.insets = new Insets(0, 0, 5, 5);
		gbc_btnConnexion.gridx = 5;
		gbc_btnConnexion.gridy = 5;
		contentPane.add(btnConnexion, gbc_btnConnexion);
		
		JLabel lblIdentifiant = new JLabel("Identifiant");
		GridBagConstraints gbc_lblIdentifiant = new GridBagConstraints();
		gbc_lblIdentifiant.anchor = GridBagConstraints.EAST;
		gbc_lblIdentifiant.insets = new Insets(0, 0, 5, 5);
		gbc_lblIdentifiant.gridx = 3;
		gbc_lblIdentifiant.gridy = 8;
		contentPane.add(lblIdentifiant, gbc_lblIdentifiant);
		
		tf_id_new_user = new JTextField();
		GridBagConstraints gbc_tf_id_new_user = new GridBagConstraints();
		gbc_tf_id_new_user.anchor = GridBagConstraints.NORTH;
		gbc_tf_id_new_user.insets = new Insets(0, 0, 5, 5);
		gbc_tf_id_new_user.fill = GridBagConstraints.HORIZONTAL;
		gbc_tf_id_new_user.gridx = 4;
		gbc_tf_id_new_user.gridy = 8;
		contentPane.add(tf_id_new_user, gbc_tf_id_new_user);
		tf_id_new_user.setColumns(10);
		
		JLabel lblMotDePasse = new JLabel("Mot de passe");
		lblMotDePasse.setHorizontalAlignment(SwingConstants.RIGHT);
		GridBagConstraints gbc_lblMotDePasse = new GridBagConstraints();
		gbc_lblMotDePasse.anchor = GridBagConstraints.EAST;
		gbc_lblMotDePasse.insets = new Insets(0, 0, 5, 5);
		gbc_lblMotDePasse.gridx = 3;
		gbc_lblMotDePasse.gridy = 9;
		contentPane.add(lblMotDePasse, gbc_lblMotDePasse);
		
		tf_pwd_new_user = new JPasswordField();
		GridBagConstraints gbc_tf_pwd_new_user = new GridBagConstraints();
		gbc_tf_pwd_new_user.insets = new Insets(0, 0, 5, 5);
		gbc_tf_pwd_new_user.fill = GridBagConstraints.HORIZONTAL;
		gbc_tf_pwd_new_user.gridx = 4;
		gbc_tf_pwd_new_user.gridy = 9;
		contentPane.add(tf_pwd_new_user, gbc_tf_pwd_new_user);
		
		JLabel lblConfirmerMotDe = new JLabel("Confirmer Mot de passe");
		lblConfirmerMotDe.setHorizontalAlignment(SwingConstants.RIGHT);
		GridBagConstraints gbc_lblConfirmerMotDe = new GridBagConstraints();
		gbc_lblConfirmerMotDe.anchor = GridBagConstraints.EAST;
		gbc_lblConfirmerMotDe.insets = new Insets(0, 0, 5, 5);
		gbc_lblConfirmerMotDe.gridx = 3;
		gbc_lblConfirmerMotDe.gridy = 10;
		contentPane.add(lblConfirmerMotDe, gbc_lblConfirmerMotDe);
		
		tf_pwd_conf_new_user = new JPasswordField();
		GridBagConstraints gbc_tf_pwd_conf_new_user = new GridBagConstraints();
		gbc_tf_pwd_conf_new_user.insets = new Insets(0, 0, 5, 5);
		gbc_tf_pwd_conf_new_user.fill = GridBagConstraints.HORIZONTAL;
		gbc_tf_pwd_conf_new_user.gridx = 4;
		gbc_tf_pwd_conf_new_user.gridy = 10;
		contentPane.add(tf_pwd_conf_new_user, gbc_tf_pwd_conf_new_user);
		
		
		
		JButton btnAjouterUnUtilisateur = new JButton("Ajouter un utilisateur");
		btnAjouterUnUtilisateur.addActionListener(new ActionListener() {
			
			// Si l'utilisateur appui sur le bouton de cr�ation d'un nouvel utilisateur
			public void actionPerformed(ActionEvent e) {
				boolean step_verif = true;
				// V�rifier que les mots de passes concordent
				if(tf_pwd_new_user.getPassword().length == tf_pwd_conf_new_user.getPassword().length)
				{
					char pwd[] = tf_pwd_new_user.getPassword();
					char pwx_verif[] = tf_pwd_conf_new_user.getPassword();
					
					for(int i = 0; i< tf_pwd_new_user.getPassword().length; i++)
					{
						if(pwd[i] != pwx_verif[i])
						{
							step_verif = false;
						}
					}
					
					if(step_verif == true)
					{
						step_verif = false;
						BufferedReader lecture_fichier;
						
						// Parcourir le fichier de sauvegarde afin de v�rifier que l'identifiant n'est pas d�j� utilis�
						try {
							lecture_fichier = new BufferedReader(new FileReader(fichier_connexion));

							String line;
							try {
								while(((line = lecture_fichier.readLine()) != null) && (step_verif == false))
								{
									System.out.println("Line : " + line);
									System.out.println("id : " +tf_id_new_user.getText());
									System.out.println(tf_id_new_user.getText().equals(line));
									if(tf_id_new_user.getText().equals(line))
									{
											step_verif = true;
									}
								}
								
								//Fermer le fichier
								lecture_fichier.close();
							} catch (IOException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
						} catch (FileNotFoundException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						
						// Si le nom d'utilisateur n'existe pas, nous pouvons en cr�er un
						if(step_verif == false)
						{
							// On peut ajouter le nouvel utilisateur
							try {
								fwriter = new FileWriter(fichier_connexion, true);
								fwriter.write(tf_id_new_user.getText() + "\n");
								System.out.println("test : " + tf_pwd_new_user.getPassword().toString());
								// Hacher le mot de passe avant de l'�crire dans le fichier de sauvegarde
								String mdp_hash = get_SHA_1_SecurePassword(tf_pwd_new_user.getText());
								
								fwriter.write(mdp_hash+ "\n");
								
								fwriter.close();
							} catch (IOException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
							
							// Ouvrir la fen�tre principale
							fenetre_principale = new MainUI(null,tf_id_new_user.getText());
							
							
							// Afficher la fen�tre principale
							fenetre_principale.setVisible(true);
							
							// Rendre cette fen�tre invisible
							setVisible(false);
							
						}
						else
						{
							// Afficher une erreur, le nom d'utilisateur existe d�j�
							lbl_erreurs.setText("Erreur : le nom d'utilisateur est d�j� utilis�");
						}
					}
				}
				else
				{
					lbl_erreurs.setText("Erreur : les mots de passes ne sont pas identiques");
				}
			}
		});
		GridBagConstraints gbc_btnAjouterUnUtilisateur = new GridBagConstraints();
		gbc_btnAjouterUnUtilisateur.insets = new Insets(0, 0, 5, 5);
		gbc_btnAjouterUnUtilisateur.gridx = 5;
		gbc_btnAjouterUnUtilisateur.gridy = 11;
		contentPane.add(btnAjouterUnUtilisateur, gbc_btnAjouterUnUtilisateur);
		lbl_erreurs = new JLabel("");
		GridBagConstraints gbc_lbl_erreurs = new GridBagConstraints();
		gbc_lbl_erreurs.gridwidth = 2;
		gbc_lbl_erreurs.insets = new Insets(0, 0, 5, 5);
		gbc_lbl_erreurs.gridx = 3;
		gbc_lbl_erreurs.gridy = 12;
		contentPane.add(lbl_erreurs, gbc_lbl_erreurs);
		
	}
	
    
    /**
     * Hacher un mot de passe
     * @param passwordToHash Mot de passe � hacher
     * @return String mot de passe hach�s
     */
    private String get_SHA_1_SecurePassword(String passwordToHash)
    {
        String generatedPassword = null;
        try {
        	MessageDigest md = MessageDigest.getInstance("SHA-256");
            byte[] bytes = md.digest(passwordToHash.getBytes());
            StringBuilder sb = new StringBuilder();
            for(int i=0; i< bytes.length ;i++)
            {
                sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
            }
            generatedPassword = sb.toString();
        }
        catch (NoSuchAlgorithmException e)
        {
            e.printStackTrace();
        }
        return generatedPassword;
    }

}
