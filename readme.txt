﻿Lancer le programme en double-cliquant sur le programme BL2P.jar.

Ce programme utilise librement les bibliothèques JFreeChart pour la génération des graphiques
ainsi que iText pour la création des rapports au format PDF. 

Afin de respecter la licence AGPL imposée par l'utilisation de iText, le code source de l'application est disponible gratuitement. 