# A Lire #


### Description du projet ###

Ce répertoire contient le projet Java à réaliser dans le cadre du projet Informatique de FIPA 2.   
Le cahier des charges du programme à réaliser est le suivant : 

Contexte :  
Une toute récente start-up souhaite, afin de gérer au mieux ses ressources financières, disposer d’une application lui permettant de gérer son portefeuille.  
Pour cela, elle fait appel à la société BL2P, spécialisée dans le développement d’applications diverses.  

Objectif :  

Les exigences de la start-up pour l’application en question sont les suivantes :  

*    Possibilité de consulter plusieurs comptes (comptes : courant, chèque, comptes épargne).
*    Possibilité de visualiser et d’éditer les mouvements financiers sur ces comptes (débits-crédits).
*    Création de rapports de suivi de budget (camembert, histogramme).
*    L’interface graphique doit être lisible et intuitive.

D’autres objectifs peuvent-être réalisés en fonction de l’avancement du projet :  

*    Possibilité de planifier les mouvements financiers futurs et de visualiser les soldes prévisionnels.
*    Envisager de coupler un agenda avec l’exigence précédente.
*    Ajouter un mode permettant de sauvegarder ses données en lignes et de les retrouver partout.


### Mettre en place git sous Windows ###

Suivre ce tuto : https://confluence.atlassian.com/bitbucket/set-up-git-744723531.html  
Puis, créer un dossier dans le workspace de Eclipse. Ce dossier va contenir le projet.   
Rendez vous dans ce dossier puis cliquer sur clique droit dans le dossier (qui est actuellement vide) puis sélectionner Git Bash dans le menu déroulant.   
Dans le terminal qui s'ouvre, taper :   
**git init**  
**git clone -b master https://VOTRE_USERNAME@bitbucket.org/guillaumebitaud/java-project.git**  
 

### Guide d'utilisation de git ###

Git permet de tracer toutes les modifications effectuées sur le code source de l'application.  
Il permet également de pouvoir travailler en équipe sans se gêner.   

Les étapes de développement sont les suivantes :  

* git pull  
La commande git pull permet de récupérer la dernière version du code source. il faut exécuter cette commande avant d'effectuer une modification
 
* git add .  
La commande git add . permet d'ajouter les fichiers modifiés au prochain commit.

* git commit -m "Message de description du commit"  
La commande git commit permet de sauvegarder (en local) les modifications effectuées sur le code source depuis le dernier commit. 
Il est important de faire des commit dès qu'une modification est effectuée sur le code source. Il faut également fournir un message de description de la modification.

* git push origin master  
Cette commande permet de sauvegarder toutes les modifications (tous les commit) sur le serveur. Attention, faire des git push seulement lorsque les modifications effectuées en locale fonctionnes.

### Collaborateurs ###

Pierre LE POTTIER  
Dorian PERON  
François LE GOFF  
Guillaume BITAUD (guillaumebitaud => administrateur)  
Guillaume BITAUD (gbitaudFIPA)